<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Presencesys | Login</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{HTML::style('assets/lte/vendor/bootstrap/css/bootstrap.min.css')}}
        {{HTML::style('assets/lte/vendor/font-awesome/css/font-awesome.min.css')}}
        {{HTML::style('assets/lte/css/AdminLTE.css')}}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">Login</div>
            {{ Form::open(array('url' => 'login')) }}
            @if ($errors->has('login'))
                <div style="margin-bottom:-15px; margin-top:-15px"class="alert alert-error">{{$errors->first('login',':message')}}</div>
            @endif
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="Username"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                    </div>     
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">Login</button>  
                   
                    <p><a href="#">Lupa password</a></p>
                </div>
            {{Form::close()}}
        </div>

        {{HTML::script('assets/lte/vendor/jquery.min.js')}}
        {{HTML::script('assets/lte/vendor/bootstrap/js/bootstrap.min.js')}}

    </body>
</html>