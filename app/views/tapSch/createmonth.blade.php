@section('pagehead')
<h1>
<i class="icon-chevron-left"></i>
	Data Jadwal
	<small>Tambah Data Jadwal</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('tapsch')}}">Jadwal</a></li>
	<li><a>Tambah Data Jadwal</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("tapsch-month"), 'onsubmit' => 'return checkDate()', 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group">
						<label>Tanggal</label>
                        <div class="input-group">
                        	<input type="text" name="rangedate" placeholder="Rentang waktu" class="form-control" style="width:500px !important;" id="reservation"/>
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->



					<div class="form-group">
						<input type="button" id="collapsible" class="btn btn-success" data-toggle="collapse" data-target="#intro" value="+ Murid"></input>
						<input type="hidden" id="kelas" class="form-control" name="kelas" value="1">
					</div>
					<div class="row-fluid summary">
    					<div id="intro" class="collapse"> 
							<div class="form-group">
								<label>Jenis jadwal</label>
								<select  name="tipe" class="form-control" required>
		                            @foreach($typesch as $key2=> $list2)
		                            <option  value="{{$list2->name}}" >{{$list2->name}}</option>
		                            @endforeach
                        		</select>
							</div>
							<div class="form-group">
								<label>Dari</label>
								<div class="input-group">
		                            <div class="input-group-addon">
		                                <i class="fa fa-clock-o"></i>
		                            </div>
		                            <div class="input-append bootstrap-timepicker">
										<input id="timepickerstart1" type="text" class="form-control" name="intervalbawah" value="{{Input::old('intervalbawah')}}" required>
							        </div>
		                        </div><!-- /.input group -->
							</div>

							<div class="form-group">
								<label>Sampai</label>
								<div class="input-group">
		                            <div class="input-group-addon">
		                                <i class="fa fa-clock-o"></i>
		                            </div>
		                            <div class="input-append bootstrap-timepicker">
										<input id="timepickerstart2" type="text" class="form-control" name="intervalatas" value="{{Input::old('intervalatas')}}" required>
							        </div>
		                        </div><!-- /.input group -->
							    <p class="help-block" id="petunjuk"></p>
							</div>
						</div>
					</div>

					

					<!-- textarea -->
					<div class="form-group">
						<label>Catatan untuk kartu</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan">{{Input::old('note')}}</textarea>
					</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop

@section('addjs')
<script type="text/javascript">
    $('#timepickerstart1').timepicker({
        minuteStep: 1,
        template: 'modal',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: '00:01'
    });

    $('#timepickerstart2').timepicker({
        minuteStep: 1,
        template: 'modal',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: '00:01'
    });

    function checkDate() 
	{
		var date1 = $('#tanggal').val()+" "+$("#timepickerstart1").val();
		var date2 = $('#tanggal').val()+" "+$("#timepickerstart2").val();

		if( (new Date(date1).getTime() > new Date(date2).getTime()))
		{
			$('#petunjuk').append('gunakan jam selesai lebih besar dari jam mulai');
			return false;
		}
		else
		{
			return true;
		}
	}
</script>
@stop