@section('pagehead')
<h1>
<i class="icon-chevron-left"></i>
	Data Jadwal
	<small>Tambah Data Jadwal</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('tapsch')}}">Jadwal</a></li>
	<li><a>Tambah Data Jadwal</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			
					<!-- text input -->
		<form method='post' id='userform' action='{{url("tapsch-remove")}}'>
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover" >
					<thead>
						<tr>
							<th>#</th>
							<th>Jenis Jadwal</th>
							<th><button style="margin-right:10px" class="btn btn-danger">{{Form::checkbox('id[]' , 'all')}} Delete All</button></th>
						
						</tr>
					</thead>
					<tbody>
						@foreach($type as $key=>$list)
						<tr>
							<td>{{$key+1}}</td>
							<td>{{$list->name}}</td>
							<td>
								<button style="margin-right:10px" class="btn btn-sm btn-default">{{Form::checkbox('id[]' , $list->id)}} Delete</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div><!-- /.box-body -->
		</form>
			{{ Form::open(array('url' => url("tapsch-add")) )}}
			<div class="box-body">
                    <div class="form-group">
						<label>Jenis jadwal</label>
						<input type="text" class="form-control" name="tipe"  required>
					</div>

					
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Tambah</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop
