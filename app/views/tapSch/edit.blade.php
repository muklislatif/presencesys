@section('pagehead')
<h1>
<i class="icon-chevron-left"></i>
	Data Jadwal
	<small>Edit Data Jadwal</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('tapsch')}}">Jadwal</a></li>
	<li><a>Edit Data Jadwal</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("tapsch/$schedule->id"), "method"=> "put", 'onsubmit' => 'return checkDate()', 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group">
						<label>Tanggal</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" id="tanggal" class="form-control pull-right datepicker" name="date" value="{{$schedule->date}}" required/>
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->

                    <div class="form-group">
						<label>Jenis jadwal</label>
						<input type="text" class="form-control" name="tipe" value="{{$schedule->type}}" required>
					</div>

					<div class="form-group">
						<label>Dari</label>
						<div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="input-append bootstrap-timepicker">
								<input id="timepickerstart1" type="text" class="form-control" name="intervalbawah" value="{{substr($schedule->startTapTime, -8, 5)}}" required>
					        </div>
                        </div><!-- /.input group -->
					</div>

					<div class="form-group">
						<label>Sampai</label>
						<div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="input-append bootstrap-timepicker">
								<input id="timepickerstart2" type="text" class="form-control" name="intervalatas" value="{{substr($schedule->finishTapTime, -8, 5)}}"  required>
					        </div>
                        </div><!-- /.input group -->
					    <p class="help-block" id="petunjuk"></p>
					</div>

					<div class="form-group">
						<label>Kelas</label>
						<select name="class" class="form-control" required>
							@foreach($class as $key=>$list)
							<option value="{{$list->id}}" {{$list->id==$schedule->idClass?'checked':''}}>{{$list->name}}</option>
							@endforeach
						</select>
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Catatan untuk kartu</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan">{{$schedule->note}}</textarea>
					</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop

@section('addjs')
<script type="text/javascript">
    $('#timepickerstart1').timepicker({
        minuteStep: 1,
        template: 'modal',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: '00:01'
    });

    $('#timepickerstart2').timepicker({
        minuteStep: 1,
        template: 'modal',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: '00:01'
    });

    function checkDate() 
	{
		var date1 = $('#tanggal').val()+" "+$("#timepickerstart1").val();
		var date2 = $('#tanggal').val()+" "+$("#timepickerstart2").val();

		if( (new Date(date1).getTime() > new Date(date2).getTime()))
		{
			$('#petunjuk').append('gunakan jam selesai lebih besar dari jam mulai');
			return false;
		}
		else
		{
			return true;
		}
	}
</script>
@stop