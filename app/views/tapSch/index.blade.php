@section('pagehead')
<h1>
	Daftar Jadwal
	<small>Jadwal yang terdaftar pada System Presence</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a>Jadwal</a></li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-tools">
					{{ Form::open(array('url' => url("tapsch-search"), 'role' => 'form')) }}
					<div class="input-group">
						<!--<a href='{{url("tapsch/create")}}' class="btn btn-sm btn-primary" style="color:white;"><strong>Tambah</strong></a> &nbsp;-->
						<a href='{{url("tapsch-month")}}' class="btn btn-sm btn-primary" style="color:white;"><strong>Tambah Jadwal</strong></a>&nbsp;
						<a href='{{url("tapsch-type")}}' class="btn btn-default" style="color:grey;"><strong>Tambah Jenis</strong></a>

						<input type="text" value="{{isset($keyword)?$keyword:''}}" name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search">
						<div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						</div>
					</div>
					{{Form::close()}}
					<br>
					{{ Form::open(array('url' => url("tapsch-filter"), 'role' => 'form')) }}
					<!--<div class="input-group">
						<button style="margin-right:10px" class="btn btn-sm btn-default"><i class="fa fa-filter"></i></button>
							@if(sizeof($kelas)>0)
							@foreach($kelas as $key=>$list)
								{{ Form::checkbox('filter',$list->name)}} {{$list->name}}
							@endforeach
							@else
							<option value="">Tidak ada kelas</option>
							@endif
					</div>-->
					{{Form::close()}}
					
				</div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover" >
					<thead>
						<tr>
							<th>#</th>
							
							<th>Tanggal</th>
							<th>Waktu Mulai</th>
							<th>Waktu Selesai</th>
							<th>Group</th>
							<th>Jenis Jadwal</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($schedule as $key=>$list)
						<tr>
							<td>{{$key+1}}</td>
							
							<td>{{$list->date}}</td>
							<td>{{date("H:i:s", strtotime($list->startTapTime))}}</td>
							<td>{{date("H:i:s", strtotime($list->finishTapTime))}}</td>
							<td>
								@if($list->idClass == 1)
									<span class="label label-primary">Guru</span>
								@else
									<span class="label label-success">Murid</span>
								@endif


							</td>
							<td>{{$list->type}}</td>
							<td>
								{{ Form::open(array('url' => url("tapsch/$list->id"), 'role' => 'form', 'method'=>'delete')) }}
								@if(strtotime($list->startTapTime) > strtotime(date("Y-m-d H:i:s")))
								<a class="btn btn-info btn-sm" href='{{url("tapsch/$list->id/edit")}}'><i class="fa fa-edit"></i> Edit</a>
								@endif
								<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Hapus Jadwal?')"> <i class="fa fa-fw fa-scissors"></i>  Delete</button>
								{{ Form::close() }}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				{{$schedule->links()}}
			</div>
		</div><!-- /.box -->
	</div>
</div>
@stop