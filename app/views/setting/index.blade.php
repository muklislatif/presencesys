@extends('layouts.master')

@section('pagehead')
<h1>
	Setting System Presensi
	<small>Daftar setting pada system presensi</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('setting')}}">Setting</a></li>
</ol>
@stop

@section('content')
<div class="row">
	<!-- left column -->
	<div class="col-md-6">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Quick Example</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form role="form">
				<div class="box-body">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleInputFile">File input</label>
						<input type="file" id="exampleInputFile">
						<p class="help-block">Example block-level help text here.</p>
					</div>
					<div class="checkbox">
						<label>
							<div class="icheckbox_minimal" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> Check me out
						</label>
					</div>
				</div><!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div><!-- /.box -->

		<!-- Form Element sizes -->
		<div class="box box-success">
			<div class="box-header">
				<h3 class="box-title">Different Height</h3>
			</div>
			<div class="box-body">
				<input class="form-control input-lg" type="text" placeholder=".input-lg">
				<br>
				<input class="form-control" type="text" placeholder="Default input">
				<br>
				<input class="form-control input-sm" type="text" placeholder=".input-sm">
			</div><!-- /.box-body -->
		</div><!-- /.box -->

		<div class="box box-danger">
			<div class="box-header">
				<h3 class="box-title">Different Width</h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-3">
						<input type="text" class="form-control" placeholder=".col-xs-3">
					</div>
					<div class="col-xs-4">
						<input type="text" class="form-control" placeholder=".col-xs-4">
					</div>
					<div class="col-xs-5">
						<input type="text" class="form-control" placeholder=".col-xs-5">
					</div>
				</div>
			</div><!-- /.box-body -->
		</div><!-- /.box -->

		<!-- Input addon -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Input Addon</h3>
			</div>
			<div class="box-body">
				<div class="input-group">
					<span class="input-group-addon">@</span>
					<input type="text" class="form-control" placeholder="Username">
				</div>
				<br>
				<div class="input-group">
					<input type="text" class="form-control">
					<span class="input-group-addon">.00</span>
				</div>
				<br>
				<div class="input-group">
					<span class="input-group-addon">$</span>
					<input type="text" class="form-control">
					<span class="input-group-addon">.00</span>
				</div>

				<h4>With icons</h4>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<input type="text" class="form-control" placeholder="Email">
				</div>
				<br>
				<div class="input-group">
					<input type="text" class="form-control">
					<span class="input-group-addon"><i class="fa fa-check"></i></span>
				</div>
				<br>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
					<input type="text" class="form-control">
					<span class="input-group-addon"><i class="fa fa-ambulance"></i></span>
				</div>

				<h4>With checkbox and radio inputs</h4>
				<div class="row">
					<div class="col-lg-6">
						<div class="input-group">
							<span class="input-group-addon">
								<div class="icheckbox_minimal" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
							</span>
							<input type="text" class="form-control">
						</div><!-- /input-group -->
					</div><!-- /.col-lg-6 -->
					<div class="col-lg-6">
						<div class="input-group">
							<span class="input-group-addon">
								<div class="iradio_minimal" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
							</span>
							<input type="text" class="form-control">
						</div><!-- /input-group -->
					</div><!-- /.col-lg-6 -->
				</div><!-- /.row -->

				<h4>With buttons</h4>
				<p class="margin">Large: <code>.input-group.input-group-lg</code></p>
				<div class="input-group input-group-lg">
					<div class="input-group-btn">
						<button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">Action <span class="fa fa-caret-down"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
						</ul>
					</div><!-- /btn-group -->
					<input type="text" class="form-control">
				</div><!-- /input-group -->
				<p class="margin">Normal</p>
				<div class="input-group">
					<div class="input-group-btn">
						<button type="button" class="btn btn-danger">Action</button>
					</div><!-- /btn-group -->
					<input type="text" class="form-control">
				</div><!-- /input-group -->
				<p class="margin">Small <code>.input-group.input-group-sm</code></p>
				<div class="input-group input-group-sm">
					<input type="text" class="form-control">
					<span class="input-group-btn">
						<button class="btn btn-info btn-flat" type="button">Go!</button>
					</span>
				</div><!-- /input-group -->
			</div><!-- /.box-body -->
		</div><!-- /.box -->

	</div><!--/.col (left) -->
	<!-- right column -->
	<div class="col-md-6">
		<!-- general form elements disabled -->
		<div class="box box-warning">
			<div class="box-header">
				<h3 class="box-title">General Elements</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<form role="form">
					<!-- text input -->
					<div class="form-group">
						<label>Text</label>
						<input type="text" class="form-control" placeholder="Enter ...">
					</div>
					<div class="form-group">
						<label>Text Disabled</label>
						<input type="text" class="form-control" placeholder="Enter ..." disabled="">
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Textarea</label>
						<textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
					</div>
					<div class="form-group">
						<label>Textarea Disabled</label>
						<textarea class="form-control" rows="3" placeholder="Enter ..." disabled=""></textarea>
					</div>

					<!-- input states -->
					<div class="form-group has-success">
						<label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Input with success</label>
						<input type="text" class="form-control" id="inputSuccess" placeholder="Enter ...">
					</div>
					<div class="form-group has-warning">
						<label class="control-label" for="inputWarning"><i class="fa fa-warning"></i> Input with warning</label>
						<input type="text" class="form-control" id="inputWarning" placeholder="Enter ...">
					</div>
					<div class="form-group has-error">
						<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Input with error</label>
						<input type="text" class="form-control" id="inputError" placeholder="Enter ...">
					</div>

					<!-- checkbox -->
					<div class="form-group">
						<div class="checkbox">
							<label>
								<div class="icheckbox_minimal" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
								Checkbox 1
							</label>
						</div>

						<div class="checkbox">
							<label>
								<div class="icheckbox_minimal" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
								Checkbox 2
							</label>
						</div>

						<div class="checkbox">
							<label>
								<div class="icheckbox_minimal disabled" aria-checked="false" aria-disabled="true" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
								Checkbox disabled
							</label>
						</div>
					</div>

					<!-- radio -->
					<div class="form-group">
						<div class="radio">
							<label>
								<div class="iradio_minimal checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
								Option one is this and that—be sure to include why it's great
							</label>
						</div>
						<div class="radio">
							<label>
								<div class="iradio_minimal" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
								Option two can be something else and selecting it will deselect option one
							</label>
						</div>
						<div class="radio">
							<label>
								<div class="iradio_minimal disabled" aria-checked="false" aria-disabled="true" style="position: relative;"><input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
								Option three is disabled
							</label>
						</div>
					</div>

					<!-- select -->
					<div class="form-group">
						<label>Select</label>
						<select class="form-control">
							<option>option 1</option>
							<option>option 2</option>
							<option>option 3</option>
							<option>option 4</option>
							<option>option 5</option>
						</select>
					</div>
					<div class="form-group">
						<label>Select Disabled</label>
						<select class="form-control" disabled="">
							<option>option 1</option>
							<option>option 2</option>
							<option>option 3</option>
							<option>option 4</option>
							<option>option 5</option>
						</select>
					</div>

					<!-- Select multiple-->
					<div class="form-group">
						<label>Select Multiple</label>
						<select multiple="" class="form-control">
							<option>option 1</option>
							<option>option 2</option>
							<option>option 3</option>
							<option>option 4</option>
							<option>option 5</option>
						</select>
					</div>
					<div class="form-group">
						<label>Select Multiple Disabled</label>
						<select multiple="" class="form-control" disabled="">
							<option>option 1</option>
							<option>option 2</option>
							<option>option 3</option>
							<option>option 4</option>
							<option>option 5</option>
						</select>
					</div>

				</form>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!--/.col (right) -->
</div>



<div class="row">
	<div class="col-md-6">

		<div class="box box-danger">
			<div class="box-header">
				<h3 class="box-title">Input masks</h3>
			</div>
			<div class="box-body">
				<!-- Date dd/mm/yyyy -->
				<div class="form-group">
					<label>Date masks:</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
					</div><!-- /.input group -->
				</div><!-- /.form group -->

				<!-- Date mm/dd/yyyy -->
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask="">
					</div><!-- /.input group -->
				</div><!-- /.form group -->

				<!-- phone mask -->
				<div class="form-group">
					<label>US phone mask:</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-phone"></i>
						</div>
						<input type="text" class="form-control" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
					</div><!-- /.input group -->
				</div><!-- /.form group -->

				<!-- phone mask -->
				<div class="form-group">
					<label>Intl US phone mask:</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-phone"></i>
						</div>
						<input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask="">
					</div><!-- /.input group -->
				</div><!-- /.form group -->

				<!-- IP mask -->
				<div class="form-group">
					<label>IP mask:</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-laptop"></i>
						</div>
						<input type="text" class="form-control" data-inputmask="'alias': 'ip'" data-mask="">
					</div><!-- /.input group -->
				</div><!-- /.form group -->

			</div><!-- /.box-body -->
		</div><!-- /.box -->

		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Color &amp; Time Picker</h3>
			</div>
			<div class="box-body">
				<!-- Color Picker -->
				<div class="form-group">
					<label>Color picker:</label>
					<input type="text" class="form-control my-colorpicker1 colorpicker-element">
				</div><!-- /.form group -->

				<!-- Color Picker -->
				<div class="form-group">
					<label>Color picker with addon:</label>
					<div class="input-group my-colorpicker2 colorpicker-element">
						<input type="text" class="form-control">
						<div class="input-group-addon">
							<i style="background-color: rgb(0, 0, 0);"></i>
						</div>
					</div><!-- /.input group -->
				</div><!-- /.form group -->

				<!-- time Picker -->
				<div class="bootstrap-timepicker"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-up"></i></a></td></tr><tr><td><span class="bootstrap-timepicker-hour">01</span></td> <td class="separator">:</td><td><span class="bootstrap-timepicker-minute">15</span></td> <td class="separator">&nbsp;</td><td><span class="bootstrap-timepicker-meridian">PM</span></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-down"></i></a></td></tr></tbody></table></div>
				<div class="form-group">
					<label>Time picker:</label>
					<div class="input-group">
						<input type="text" class="form-control timepicker">
						<div class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</div>
					</div><!-- /.input group -->
				</div><!-- /.form group -->
			</div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->

</div><!-- /.col (left) -->
<div class="col-md-6">
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Date picker</h3>
		</div>
		<div class="box-body">
			<!-- Date range -->
			<div class="form-group">
				<label>Date range:</label>
				<div class="input-group">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</div>
					<input type="text" class="form-control pull-right" id="reservation">
				</div><!-- /.input group -->
			</div><!-- /.form group -->

			<!-- Date and time range -->
			<div class="form-group">
				<label>Date and time range:</label>
				<div class="input-group">
					<div class="input-group-addon">
						<i class="fa fa-clock-o"></i>
					</div>
					<input type="text" class="form-control pull-right" id="reservationtime">
				</div><!-- /.input group -->
			</div><!-- /.form group -->

			<!-- Date and time range -->
			<div class="form-group">
				<label>Date range button:</label>
				<div class="input-group">
					<button class="btn btn-default pull-right" id="daterange-btn">
						<i class="fa fa-calendar"></i> Date range picker
						<i class="fa fa-caret-down"></i>
					</button>
				</div>
			</div><!-- /.form group -->

		</div><!-- /.box-body -->
	</div><!-- /.box -->

	<!-- iCheck -->
	<div class="box box-success">
		<div class="box-header">
			<h3 class="box-title">iCheck - Checkbox &amp; Radio Inputs</h3>
		</div>
		<div class="box-body">
			<!-- Minimal style -->

			<!-- checkbox -->
			<div class="form-group">
				<label>
					<div class="icheckbox_minimal checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="minimal" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="icheckbox_minimal" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="minimal" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="icheckbox_minimal disabled" aria-checked="false" aria-disabled="true" style="position: relative;"><input type="checkbox" class="minimal" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
					Minimal skin checkbox
				</label>
			</div>

			<!-- radio -->
			<div class="form-group">
				<label>
					<div class="iradio_minimal checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r1" class="minimal" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="iradio_minimal" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r1" class="minimal" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="iradio_minimal disabled" aria-checked="false" aria-disabled="true" style="position: relative;"><input type="radio" name="r1" class="minimal" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
					Minimal skin radio
				</label>
			</div>

			<!-- Minimal red style -->

			<!-- checkbox -->
			<div class="form-group">
				<label>
					<div class="icheckbox_minimal-red checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="minimal-red" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="icheckbox_minimal-red" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="minimal-red" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="icheckbox_minimal-red disabled" aria-checked="false" aria-disabled="true" style="position: relative;"><input type="checkbox" class="minimal-red" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
					Minimal red skin checkbox
				</label>
			</div>

			<!-- radio -->
			<div class="form-group">
				<label>
					<div class="iradio_minimal-red checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r2" class="minimal-red" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="iradio_minimal-red" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r2" class="minimal-red" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="iradio_minimal-red disabled" aria-checked="false" aria-disabled="true" style="position: relative;"><input type="radio" name="r2" class="minimal-red" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
					Minimal red skin radio
				</label>
			</div>

			<!-- Minimal red style -->

			<!-- checkbox -->
			<div class="form-group">
				<label>
					<div class="icheckbox_flat-red checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="flat-red" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="icheckbox_flat-red" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="flat-red" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="icheckbox_flat-red disabled" aria-checked="false" aria-disabled="true" style="position: relative;"><input type="checkbox" class="flat-red" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
					Flat red skin checkbox
				</label>
			</div>

			<!-- radio -->
			<div class="form-group">
				<label>
					<div class="iradio_flat-red checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="iradio_flat-red" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
				</label>
				<label>
					<div class="iradio_flat-red disabled" aria-checked="false" aria-disabled="true" style="position: relative;"><input type="radio" name="r3" class="flat-red" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
					Flat red skin radio
				</label>
			</div>
		</div><!-- /.box-body -->
		<div class="box-footer">
			Many more skins available.
		</div>
	</div><!-- /.box -->
</div><!-- /.col (right) -->
</div>
@stop