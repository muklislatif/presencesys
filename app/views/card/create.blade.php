@section('pagehead')
<h1>
	Data Kartu
	<small>Menambahkan Kartu</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('card')}}">Kartu</a></li>
	<li><a>Tambah Kartu</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url('card'), 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Id Kartu</label>
						<input type="text" class="form-control" name="cardnum" required>
					</div>

					<!-- radio -->
					<div class="form-group"> 
						<label>Status Kartu</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" value="1" checked>
                                Aktif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" value="0">
                                Tidak aktif
                            </label>
                        </div>
                    </div>

					<!-- textarea -->
					<div class="form-group">
						<label>Catatan untuk kartu</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan"></textarea>
					</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop