@section('pagehead')
<h1>
	Daftar Kartu
	<small>Daftar kartu yang terdaftar di system presensi</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a>Kartu</a></li>

</ol>
@stop

@section('content')

<div class="row">
<!--	<div class="col-lg-3 col-xs-6">
		<!-- small box 
		<a href="{{url('card/create')}}">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h2>
						Tambah Kartu
					</h2>
					<p>
						Total kartu
					</p>
				</div>
				<div class="icon">
					<i class="fa fa-fw fa-credit-card"></i>
				</div>
				<div class="small-box-footer">
					 <i class="fa fa-arrow-circle-right"></i>
				</div>
			</div>
		</a>
	</div>--><!-- ./col -->

	<!--<div class="col-lg-3 col-xs-6">
			<!-- small box
			<a href="#">
			<div class="small-box bg-green">
				<div class="inner">
					<h3>
						Import Kartu
					</h3>
					<p>
						Menambah kartu sekaligus
					</p>
				</div>
				<div class="icon">
					<i class="fa fa-fw fa-ruble"></i>
				</div>
				<div class="small-box-footer">
					<i class="fa fa-arrow-circle-right"></i>
				</div>
			</div>
		</a>
		</div>-->

</div>

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">

				<div class="box-tools">
					{{ Form::open(array('url' => url("card-search"), 'role' => 'form')) }}
					<div class="input-group">
						<input type="text" value="{{isset($keyword)?$keyword:''}}" name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search">
						<div class="input-group-btn">
							<button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						</div>
					</div>
					{{ Form::close() }}
				</div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tbody><tr>
						<th>#</th>
						<th>Card Number</th>
						<th>Status</th>
						<th>Note</th>
						<th>Action</th>
					</tr>

					@if(!empty($data))
					@foreach($data as $key=>$list)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$list->cardNum}}</td>
						<td>
							@if($list->status==1)
							<span class="label label-success">Aktif</span>
							@elseif($list->status==0)
							<span class="label label-danger">Tidak aktif</span>
							@endif
						</td>
						<td>{{$list->note}}</td>
						<td>
							{{ Form::open(array('url' => url("card/$list->id"), 'role' => 'form', 'method'=>'delete')) }}
							<a class="btn btn-info btn-sm" href='{{url("card/$list->id/edit")}}'><i class="fa fa-edit"></i> Edit</a>
							<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Hapus kartu?')"> <i class="fa fa-fw fa-scissors"></i>  Delete</button>
							{{ Form::close() }}
						</td>
					</tr>
					@endforeach
					@endif
				</tbody></table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				{{$data->links()}}
			</div>
		</div><!-- /.box -->
	</div>
</div>
@stop