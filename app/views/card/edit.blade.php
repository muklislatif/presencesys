@section('pagehead')
<h1>
	Data Kartu
	<small>Edit Data Kartu</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('card')}}">Kartu</a></li>
	<li><a>Edit Kartu</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("card/$card->id"), 'method' => 'put', 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Id Kartu</label>
						<input type="text" class="form-control" name="cardnum" value="{{$card->cardNum}}" required>
					</div>

					<!-- radio -->
					<div class="form-group"> 
						<label>Status Kartu</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" value="1" {{$card->status==1?'checked':''}}>
                                Aktif
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" value="0" {{$card->status==0?'checked':''}}>
                                Tidak aktif
                            </label>
                        </div>
                    </div>

					<!-- textarea -->
					<div class="form-group">
						<label>Catatan untuk kartu</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan">{{$card->note}}</textarea>
					</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop