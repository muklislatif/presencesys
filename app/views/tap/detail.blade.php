s@section('pagehead')
<h1>
	Data Kelas
	<small>Presensi</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('class')}}">Kelas</a></li>
	<li><a>Presensi Kelas</a></li>
</ol>
@stop

@section('content')
<style>
.datepicker{z-index:2000 !important;}
</style>
<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Nama Kelas</label>
						<p>{{$class->name}}</p>
					</div>
			</div><!-- /.box-body -->

			<div class="box-body">
				<h4>Daftar Presensi</h4>
				<div class="box-header">
					{{ Form::open(array('url' => url("tap-search/$class->id"), 'role' => 'form', 'data-parsley-validate')) }}
					<div class="box-tools">
						<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Export</button>-->
						<button type="button" class="btn btn-primary" onclick=window.print()>Export</button>
						<div class="input-group col-md-1 col-md-offset-1 pull-right">
                        	<button class="btn btn-primary" type="submit">Cari</button>
						</div>
						<div class="input-group col-md-3 pull-right">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="rangedate" placeholder="Rentang waktu" class="form-control pull-right" id="reservation"/>
                        </div>
					</div>
					{{Form::close()}}
				</div><!-- /.box-header -->
				<hr>
				
				@foreach($schedules as $key=>$list)
				<div class="well"><strong>{{$list->date}} || ({{strtoupper($list->type)}}) </strong>
				<!--{{$key = 0}}-->
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>No.</th>
								<th>Taps</th>
								<th>NIS</th>
								<th>Nama Murid</th>
								<th>Tanggal</th>
								<th>Kehadiran</th>
								
							</tr>
						@foreach($shows as $key2 =>$list2)
							<tr>
								@if($list->id == $list2->idTapSch)
								<td>{{$key = $key+1}}</td>
								<td>{{$list2->updated_at}}</td>
								<td>{{$list2->NIS}}</td>
								<td>{{ucwords(strtolower($list2->name))}}</td>
								<td>{{$list->date}}</td>
								<td> {{$list->type}}  &nbsp
								
									@if($list2->status==1)
									<!--<span class="label label-success">Hadir</span>-->
									<i class="fa fa-check-square fa-2x">
									@elseif($list2->status==2)
									<!--<span class="label label-danger">Tidak hadir</span>-->
									<i class="fa fa-minus-square fa-2x"></i>
									@elseif($list2->status==3)
									<span class="label label-success">Sakit</span>
									@elseif($list2->status==4)
									<span class="label label-success">Izin</span>
									@elseif($list2->status==0)
									<span class="label label-danger">Tidak hadir</span>
									@endif
								</td>
								@endif
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>

				@endforeach
				
				
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			{{ Form::open(array('url' => url("tap-export/$class->id"), 'onsubmit' => 'return checkDate()', 'role' => 'form', 'target' => '_blank', 'data-parsley-validate')) }}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Export Data Presensi</h4>
			</div>
			<div class="modal-body">
				<div class="form-group"> 
					<label>Export ke</label>
					<div class="radio">
                        <label>
                            <input type="radio" name="type" value="xls" checked>
                            Excel
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="type" value="pdf">
                            PDF
                        </label>
                    </div>
				</div>
				<div class="form-group">
                    <label>Dari tanggal</label>
                    <input type="text" name="from" id="from" class="form-control datepicker2"  data-date-format="yyyy-mm-dd">
                </div>
                <div class="form-group">
                    <label>Hingga tanggal</label>
                    <input type="text" name="until" id="until" class="form-control datepicker2"  data-date-format="yyyy-mm-dd">
                    <p class="help-block text-warning" id="petunjuk"></p>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Export</button>
			</div>
			{{Form::close()}}
		</div>
	</div>
</div>
@stop

@section('addjs')
<script type="text/javascript">
	
	function checkDate() 
	{
		var date1 = $('#from').val();
		var date2 = $('#until').val();

		if( (new Date(date1).getTime() > new Date(date2).getTime()))
		{
			$('#petunjuk').append('gunakan tanggal lebih besar dari tanggal awal');
			return false;
		}
		else
		{
			return true;
		}
	}

</script>
@stop