@section('pagehead')
@if($akun->username == 'admin' || $akun->username == $user->username)
<h1>
	Data Admin
	<small>Edit Admin</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('admin')}}">Admin</a></li>
	<li><a>Edit Admin</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("admin/$user->id"), 'method' => 'put', 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					
					<div class="form-group"> 
						<label>Nama</label>
						<input type="text" class="form-control" name="name" value="{{$user->name}}" required>
					</div>
					
					<div class="form-group"> 
						<label>Username</label>
						<input type="text" class="form-control" name="username" value="{{$user->username}}" required>
					</div>
					<div class="form-group"> 
						<label>Email</label>
						<input type="text" class="form-control" name="email" value="{{$user->email}}" required>
					</div>
					<div class="form-group"> 
						<label>Password</label>
						<input type="password" class="form-control" name="password" value="" required>
					</div>																
					<div class="form-group"> 
						<label>Confirm Password</label>
						<input type="password" class="form-control" name="cpassword" value="" required>
					</div>
					

			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			@else
				<div class="box-body">
					<center><h4> ADMIN AREA!</h4></center>
				</div>
			@endif
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop