@section('pagehead')
<h1>
	Daftar Akun
	<small>daftar akun yang dapat menggunakan System Presence</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('admin')}}">Akun</a></li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				
				<div class="box-tools">
					{{ Form::open(array('url' => url("admin-search"), 'role' => 'form')) }}
					<div class="input-group">
						@if($akun->username == 'admin')
						<a href='{{url("admin/create")}}' class="btn btn-sm btn-primary" style="color:white;"><strong>Tambah</strong></a>
						@endif
						<input type="text" name="keyword" value="{{isset($keyword)?$keyword:''}}" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search">
						<div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						</div>
					</div>
				{{Form::close()}}
				</div>
				
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tbody><tr>
						<th>No</th> 
						<th>Nama</th>
						<th></th>
					</tr>
					@if($akun->username == 'admin')
					@foreach($users as $key=>$list)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$list->name}}</td>
						<td>
							{{ Form::open(array('url' => url("admin/$list->id"), 'role' => 'form', 'method'=>'delete')) }}
							<a href='{{url("admin/$list->id/edit")}}' class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
							<button type="submit" class="btn btn-danger" onclick="return confirm('Hapus akun?')"><i class="fa fa-fw fa-scissors"></i> Hapus</button>
							{{ Form::close() }}
						</td>
					</tr>
					@endforeach
					@endif

					@if($akun->username != 'admin')
					@foreach($users as $key=>$list)
					<!--{{$key = 0}}-->
					@if($list->name == $akun->name)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$list->name}}</td>
						<td>
							{{ Form::open(array('url' => url("admin/$list->id"), 'role' => 'form', 'method'=>'delete')) }}
							<a href='{{url("admin/$list->id/edit")}}' class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
							<button type="submit" class="btn btn-danger" onclick="return confirm('Hapus akun?')"><i class="fa fa-fw fa-scissors"></i> Hapus</button>
							{{ Form::close() }}
						</td>
					</tr>
					@endif
					@endforeach
					@endif
				</tbody></table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				{{$users->links()}}
			</div>
		</div><!-- /.box -->
	</div>
</div>
@stop