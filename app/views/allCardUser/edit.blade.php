@section('pagehead')
<h1>
	Data Murid
	<small>Edit Data Murid</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('student-full')}}">Kartu</a></li>
	<li><a>Edit Kartu</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("student-full/$student->id"), 'method' => 'put', 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Nama Murid</label>
						<input type="text" class="form-control" name="name" value="{{$student->name}}" required>
					</div>

					<div class="form-group">
						<label>Nomor Induk Siswa/i</label>
						<input type="text" class="form-control" name="nis" value="{{$student->NIS}}" required>
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" name="address" rows="3" required>{{$student->address}}</textarea>
					</div>

					<div class="form-group">
						
								<label>Id Kartu</label>
								<input type="text" class="form-control" name="card" value="{{$student->card->cardNum}}" required>
								<input type="hidden" class="form-control" name="idCard" value="{{$student->card->id}}" >
							
					</div>

					<div class="form-group">
						
								<label>No Telp.</label>
								<input type="text" class="form-control" name="notif" value="{{$student->notif->phone}}" required>
								<input type="hidden" class="form-control" name="idnotif" value="{{$student->notif->id}}" required>
							
					</div>

					<!--<div class="form-group">
						<label>Notifikasi</label>
						<select name="parent" class="form-control" required>
							@if(sizeof($notif)>0)
							@foreach($notif as $list)
							<option value="{{$list->id}}" {{$list->id==$student->idNotif?'selected':''}}>{{$list->parent}} / {{$list->email}}</option>
							@endforeach
							@else
							<option value="">Tidak ada data Notifikasi</option>
							@endif
						</select>
					</div>-->
					

					<div class="form-group">
						<label>Catatan</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan">{{$student->note}}</textarea>
					</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop