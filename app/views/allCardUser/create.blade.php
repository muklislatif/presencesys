@section('pagehead')
<h1>
	Data Murid
	<small>Tambah Data Murid</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('student-full')}}">Murid</a></li>
	<li><a>Tambah Data Murid</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("student-full"), 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Nama Murid</label>
						<input type="text" class="form-control" name="name" required>
					</div>

					<div class="form-group"> 
						<label>Nomor Induk Siswa/i</label>
						<input type="text" class="form-control" name="nis" required>
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" name="address" rows="2" required></textarea>
					</div>

					<div class="form-group">
						<label>Kelas</label>
						<select name="class" class="form-control" required>
							@if(sizeof($class)>0)
							@foreach($class as $key=>$list)
							<option value="{{$list->id}}">{{$list->name}} </option>
							@endforeach
							@else
							<option value="">Tidak ada data Notifikasi</option>
							@endif
						</select>
					</div>
					<div class="form-group">
						<label>Catatan</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan"></textarea>
					</div>
					

					<div class="form-group">
						<div class="box box-primary">
						
							<label>TAMBAH KARTU</label>
							<div class="box box-primary">
							<div class="box-body">
							<!-- text input -->
							<div class="form-group"> 
								<label>Id Kartu</label>
								<input type="text" class="form-control" name="cardnum" required>
							</div>

							<!-- radio -->
							<div class="form-group"> 
								<label>Status Kartu</label>
		                        <div class="radio">
		                            <label>
		                                <input type="radio" name="status" value="1" checked>
		                                Aktif
		                            </label>
		                        </div>
		                        <div class="radio">
		                            <label>
		                                <input type="radio" name="status" value="0">
		                                Tidak aktif
		                            </label>
		                        </div>
		                    </div>

							<!-- textarea -->
							<div class="form-group">
								<label>Catatan untuk Kartu</label>
								<textarea class="form-control" name="noteCard" rows="3" placeholder="Catatan jika diperlukan"></textarea>
							</div>
							</div>
							</div>
						
					</div>

				
					

					

			<div class="box box-primary">			
			<label>DATA NOTIFIKASI</label>
			<div class="box box-primary">
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						

					<div class="form-group"> 
						<label>Email</label>
						<input type="email" class="form-control" name="email" required>
					</div>

					<div class="form-group"> 
						<label>Telepon</label>
						<input type="text" class="form-control" name="phone" required data-parsley-type="number">
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Catatan untuk Notifikasi</label>
						<textarea class="form-control" name="noteNotif" rows="3" placeholder="Catatan jika diperlukan"></textarea>
					</div>
			</div><!-- /.box-body -->
			
		</div><!-- /.box -->
		<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			</div><!-- /.box-body -->
			
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop