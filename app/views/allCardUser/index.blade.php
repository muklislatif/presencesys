
@section('pagehead')
<h1>
	Daftar Murid
	<small>Daftar Murid yang terdaftar pada System Presence</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a>Murid</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-tools">
					{{ Form::open(array('url' => url("student-full-search"), 'role' => 'form')) }}
					<div class="input-group">
						<a href='{{url("student-full/create")}}' class="btn btn-sm btn-primary" style="color:white;"><strong>Tambah</strong></a>
						<input type="text" value="{{isset($keyword)?$keyword:''}}" name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search">
						<div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						</div>
					</div>
					{{ Form::close() }}
					<br>
					<form method='post' id='userform' action='{{url("student-full-filter")}}'>
					<div  class="input-group">
					
							@if(sizeof($kelas)>0)
							<div style="overflow-x: hidden; width:78em; white-space: nowrap; "   class="scrollable">
							@foreach($kelas as $key=>$list)

								<button style="margin-right:10px" class="btn btn-sm btn-default">{{Form::checkbox('filter' , $list->name)}} {{$list->name}}</button>

							@endforeach
							</div>
							@else
							<option value="">Tidak ada Kelas</option>
							@endif
					
					</div>
					</form>
				</div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tbody>
						<tr>
							<th>#</th>
							<th>NIS</th>
							<th>Card</th>
							<th>Nama</th>
							<th>Kelas</th>
							<th>Alamat</th>
							<th></th>
						</tr>
						@foreach($student as $key=>$list)
						<tr>
							
								<td>{{$key+1}}</td>
								<td>{{$list->NIS}}</td>
								<td>{{$list->cardNum}}</td>
								<td>{{$list->name}}</td>
								@if($list->idClass === 14)
								<td>-</td>
								@else
								<td>{{$list->group->name}}</td>
								@endif
								<td>{{$list->address}}</td>
								<td>
									{{ Form::open(array('url' => url("student-full/$list->id"), 'role' => 'form', 'method'=>'delete')) }}
									<a class="btn btn-info btn-sm" href='{{url("student-full/$list->id/edit")}}'><i class="fa fa-edit"></i> Edit</a>
									<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Hapus Murid?')"> <i class="fa fa-fw fa-scissors"></i>  Delete</button>
									{{ Form::close() }}
								</td>
							
						</tr>
						@endforeach
					</tbody>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				{{$student->links()}}
			</div>
		</div><!-- /.box -->
	</div>
</div>
@stop