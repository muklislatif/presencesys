@section('pagehead')
<h1>
	Data Mesin
	<small>Edit Data Mesin</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('machine')}}">Mesin</a></li>
	<li><a>Edit Data Mesin</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("machine/$machine->id"), 'method' => 'put', 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Alamat IP</label>
						<input type="text" class="form-control" name="ip" value="{{$machine->ip}}" required>
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Catatan untuk mesin</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan">{{$machine->note}}</textarea>
					</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop