{{HTML::script('assets/lte/vendor/jquery.min.js')}}

{{HTML::script('assets/lte/vendor/bootstrap/js/bootstrap.min.js')}}

{{HTML::script('assets/lte/vendor/jquery-ui.min.js')}}

{{HTML::script('assets/lte/vendor/parsley/parsley.min.js')}}

<!-- Morris.js charts -->
{{HTML::script('assets/lte/vendor/raphael-min.js')}}

{{HTML::script('assets/lte/js/plugins/morris/morris.min.js')}}

<!-- Sparkline -->
{{HTML::script('assets/lte/js/plugins/sparkline/jquery.sparkline.min.js')}}

<!-- jvectormap -->
{{HTML::script('assets/lte/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}

{{HTML::script('assets/lte/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}

<!-- jQuery Knob Chart -->
{{HTML::script('assets/lte/js/plugins/jqueryKnob/jquery.knob.js')}}

<!-- daterangepicker -->
{{HTML::script('assets/lte/js/plugins/daterangepicker/daterangepicker.js')}}

<!-- datepicker -->
{{HTML::script('assets/lte/js/plugins/datepicker/bootstrap-datepicker.js')}}
{{HTML::script('assets/lte/js/plugins/daterangepicker/daterangepicker.js')}}

<!-- Bootstrap WYSIHTML5 -->
{{HTML::script('assets/lte/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}

<!-- iCheck -->
{{HTML::script('assets/lte/js/plugins/iCheck/icheck.min.js')}}

<!-- AdminLTE App -->
{{HTML::script('assets/lte/js/AdminLTE/app.js')}}

{{HTML::script('assets/chosen/chosen.jquery.min.js')}}

{{HTML::script('assets/timepicker/js/bootstrap-timepicker.min.js')}}

{{HTML::script('assets/js/notif.js')}}


<script type="text/javascript">
	(function() {
	$("#example1").dataTable();
		})

	$('.datepicker').datepicker({
	    format: 'yyyy-mm-dd',
	    startDate: '-0d'
	})
  $('.datepicker2').datepicker();
</script>

<script type="text/javascript">
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}

//Date range picker
$('#reservation').daterangepicker(
  {
    format: 'YYYY-MM-DD'
  });
//Date range picker with time picker
$('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'YYYY-MM-DD'});
//Date range as a button
$('#daterange-btn').daterangepicker(
        {
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            startDate: moment().subtract('days', 29),
            endDate: moment()
        },
function(start, end) {
    $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
}
);
</script>
<script type="text/javascript">
$( '.scrollable' ).bind( 'mousewheel DOMMouseScroll', function ( e ) {
    var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
    
    this.scrollLeft += ( delta < 0 ? 1 : -1 ) * 30;
    e.preventDefault();
});
</script>
<script type="text/javascript">
function handleClick()
{
    if  (this.value == '+ Murid') {
            document.getElementById("kelas").value = "0" 

    }else{
            document.getElementById("kelas").value = "1" 

    }
    this.value = (this.value == '+ Murid' ? '- Guru' : '+ Murid');
    //this.value = (this.value == '+ Murid' ? document.getElementById("kelas").value = "guru" : document.getElementById("kelas").value = "murid");
    
}
document.getElementById('collapsible').onclick=handleClick;

</script>