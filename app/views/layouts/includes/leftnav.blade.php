<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <br>
        <img src="{{url('assets/lte')}}/img/logo215.png" alt="Telkom School"/>
        <br>
        <br>

        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{url('assets/lte')}}/img/avatar3.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, {{$akun->name}}</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->


        

        
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <!-- <li class="active">
                <a href="#dashboard"><i class="fa fa-fw fa-envelope"></i> Dashboard</a>
            </li> -->
            <li class="active">
                <a href="{{url('admin')}}"><i class="fa fa-fw fa-th-list"></i> Akun</a>
            </li>
            <li class="active">
                <a href="{{url('card')}}"><i class="fa fa-fw fa-credit-card"></i> Kartu</a>
            </li>
            <!--<li class="active">
                <a href="{{url('student')}}"><i class="fa fa-fw fa-user"></i> Murid</a>
            </li>-->
            <li class="active">
                <a href="{{url('student-full')}}"><i class="fa fa-fw fa-users"></i>  Murid</a>
            </li>
            <li class="active">
                <a href="{{url('class')}}"><i class="fa fa-fw fa-home"></i> Kelas</a>
            </li>
            <li class="active">
                <a href="{{url('machine')}}"><i class="fa fa-fw fa-desktop"></i> Mesin Presensi</a>
            </li>
            <li class="active">
                <a href="{{url('tapsch')}}"><i class="fa fa-fw fa-calendar"></i> Jadwal Presensi</a>
            </li>
            <li class="active">
                <a href="{{url('rekap')}}"><i class="fa fa-fw fa-check-square-o"></i> Rekapitulasi Presensi</a>
            </li>
            <li class="active">
                <a href="{{url('parent')}}"><i class="fa fa-fw fa-bell"></i> Notifikasi</a>
            </li>
            <!-- <li class="active">
                <a href="#message"><i class="fa fa-fw fa-envelope"></i> Pesan Terkirim</a>
            </li>
            <li class="active">
                <a href="#setting"><i class="fa fa-fw fa-cogs"></i> Pengaturan</a>
            </li> -->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>    