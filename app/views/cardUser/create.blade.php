@section('pagehead')
<h1>
	Data Karyawan
	<small>Tambah Data Karyawan</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('student')}}">Karyawan</a></li>
	<li><a>Tambah Data Karyawan</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("student"), 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Nama Karyawan</label>
						<input type="text" class="form-control" name="name" required>
					</div>

					<div class="form-group"> 
						<label>Nomor Induk Karyawan</label>
						<input type="text" class="form-control" name="nis" required>
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" name="address" rows="2" required></textarea>
					</div>

					<div class="form-group">
						<label>Kartu</label>
						<select name="card" class="form-control" required>
							@if(sizeof($cards)>0)
							@foreach($cards as $list)
							<option value="{{$list->id}}">{{$list->cardNum}}</option>
							@endforeach
							@else
							<option value="">Tidak ada kartu</option>
							@endif
						</select>
					</div>

					<div class="form-group">
						<label>Notifikasi</label>
						<select name="parent" class="form-control" required>
							@if(sizeof($notif)>0)
							@foreach($notif as $list)
							<option value="{{$list->id}}">{{$list->parent}} / {{$list->email}}</option>
							@endforeach
							@else
							<option value="">Tidak ada data Notifikasi</option>
							@endif
						</select>
					</div>

					<div class="form-group">
						<label>Catatan</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan"></textarea>
					</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop