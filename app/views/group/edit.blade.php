@section('pagehead')
<h1>
	Data Kelas
	<small>Edit Data Kelas</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('class')}}">Kelas</a></li>
	<li><a>Edit Kelas</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("class-update/$class->id"), 'method' => 'post')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Nama Kelas</label>
						<input type="text" class="form-control" name="name" required value="{{$class->name}}">
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Catatan untuk kelas</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan">{{$class->note}}</textarea>
					</div>
			</div><!-- /.box-body -->

			<div style="margin-top:-20px" class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}

			
			<br>
			<div class="box-body table-responsive no-padding">
				<h4>Daftar murid dalam kelas</h4>
				<form method='post' id='userform' action='{{url("class-remove")}}'>
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>#</th>
								<th>NIS</th>
								<th>Nama Murid</th>
								<th><button style="margin-right:10px" class="btn btn-danger">{{Form::checkbox('id[]' , 'all')}} Delete All</button></th>
							</tr>
								@foreach($students as $key=>$list)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$list->NIS}}</td>
										<td>{{$list->name}}</td>
										<td>
											<button style="margin-right:10px" class="btn btn-sm btn-default">{{Form::checkbox('id[]' , $list->id)}} Delete</button>
											<input type='hidden' name='idclass' value='{{$class->id}}'>
										</td>
									</tr>
								@endforeach
						</tbody>
					</table>
				</form>
			</div><!-- /.box-body -->

			{{ Form::open(array('url' => url("class-update/$class->id"), 'method' => 'post', 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
				<div class="form-group"> 
					<label>Tambah murid kedalam kelas</label>
					<select name="studentadd[]" class="chosen-select form-control" multiple>
						@foreach($addstudents as $key=>$list)
						<option value="{{$list->id}}">{{$list->name}}</option>
						@endforeach
					</select>
				</div>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop