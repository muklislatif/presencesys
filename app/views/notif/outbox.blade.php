@section('pagehead')
<h1>
	Daftar Notifikasi
	<small>Nomor telepon orang tua murid yang terdaftar pada System Presence</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('parent')}}">Notifikasi</a></li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tbody>
						<tr>
							<th>No</th>
							<th>Tujuan</th>
							<th>Pesan</th>
							<th><center>
								@if($status == 0)
									<a class="btn btn-danger btn-sm" href='#'> SMS is off!</a>
								@else
									<a class="btn btn-info btn-sm" href='#'> SMS is on!</a>
								@endif
								</center>
							</th>
						</tr>
						@if(count($messages) > 0)
							@foreach($messages as $key=>$list)
							<tr>
								<td>{{$key+1}}</td>
								<td>{{$list->DestinationNumber}}</td>
								<td>{{$list->TextDecoded}}</td>
								<td><center>
									{{ Form::open(array('url' => url("parent"), 'role' => 'form', 'method'=>'delete')) }}
									<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Hapus Notifikasi?')"> <i class="fa fa-fw fa-scissors"></i>  Delete</button>
									{{ Form::close() }}
									</center>
								</td>
							</tr>
							@endforeach
						@else
							<h4> All messages has been sent </h4>
						@endif
					</tbody>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				{{$messages->links()}}
			</div>
		</div><!-- /.box -->
	</div>
</div>
@stop