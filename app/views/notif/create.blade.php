@section('pagehead')
<h1>
	Data Orang Tua
	<small>Menambahkan Orang Tua</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('parent')}}">Orang Tua</a></li>
	<li><a>Tambah Orang Tua</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url('parent'), 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Nama</label>
						@if(isset($student))
							<input type="text" class="form-control" name="name" value"{{$student->name}}" required>
						@else
							<input type="text" class="form-control" name="name"  required>
						@endif
					</div>

					<div class="form-group"> 
						<label>Email</label>
						<input type="email" class="form-control" name="email" required>
					</div>

					<div class="form-group"> 
						<label>Telepon</label>
						<input type="text" class="form-control" name="phone" required data-parsley-type="number">
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Catatan untuk orang tua</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan"></textarea>
					</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop