@section('pagehead')
<h1>
<i class="icon-chevron-left"></i>
	Data Jadwal
	<small>Tambah Data Jadwal</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('tapsch')}}">Jadwal</a></li>
	<li><a>Tambah Data Jadwal</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("parent-send"))) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Subject</label>
						<input type="text" class="form-control" name="subject" placeholder="Jenis pesan" required>
					</div>

                    <div class="form-group">
						<label>Jenis jadwal</label>
						<select  name="class" class="form-control" required>
								<option  value="all" >-- semua --</option>
	                            @foreach($class as $key2=> $list2)
	                            <option  value="{{$list2->id}}" >{{$list2->name}}</option>
	                            @endforeach
                        	</select>
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Isi Pesan</label>
						<textarea class="form-control" name="message" rows="3" placeholder="Masukan pesan"></textarea>
					</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop
