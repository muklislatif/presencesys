@section('pagehead')
<h1>
	Daftar Notifikasi
	<small>Nomor telepon orang tua murid yang terdaftar pada System Presence</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('parent')}}">Notifikasi</a></li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-tools">
					{{ Form::open(array('url' => url("parent-search"), 'role' => 'form')) }}
					<div class="input-group">
						<a href='{{url("parent-broadcast")}}' class="btn btn-sm btn-primary" style="color:white;"><strong>Broadcast SMS</strong></a>&nbsp;
						@if($status == 0)
							<a type="button" class="label label-danger" data-toggle="modal" data-target="#demo"><strong>SMS is OFF!</strong></a>
						@else
							<a type="button" class="label label-success" data-toggle="modal" data-target="#demo"><strong>SMS is ON</strong></a>
						@endif &nbsp;
						<a href='{{url("parent-outbox")}}' class="label label-success" style="color:white;"><strong>Outbox</strong></a> &nbsp;
						<a href='{{url("parent-pulsa")}}' class="label label-success" style="color:white;"><strong>Pulsa</strong></a> &nbsp;
						<input type="text" value="{{isset($keyword)?$keyword:''}}" name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search">
						<div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						</div>
					</div>
					{{Form::close()}}
				</div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tbody>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<!--<th>Email</th>-->
							<th>Telepon</th>
							<th></th>
						</tr>
						@foreach($notif as $key=>$list)
						<tr>
							<td>{{$key+1}}</td>
							<td>{{$list->name}}</td>
							<!--<td>{{$list->email}}</td>-->
							<td>{{$list->phone}}</td>
							<td>
								{{ Form::open(array('url' => url("parent/$list->id"), 'role' => 'form', 'method'=>'delete')) }}
								<a class="btn btn-info btn-sm" href='{{url("parent/$list->id/edit")}}'><i class="fa fa-edit"></i> Edit</a>
								<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Hapus Notifikasi?')"> <i class="fa fa-fw fa-scissors"></i>  Delete</button>
								{{ Form::close() }}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				{{$notif->links()}}
			</div>
		</div><!-- /.box -->
	</div>
</div>

<div class="modal fade" id="demo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">SMS Gateway</h4>
      </div>
      <div class="modal-body">
        <div class="media">
	        <a href="#" class="pull-left">
	            <img src="start.jpg" class="media-object" alt="Sample Image">
	        </a>
	        <div class="media-body">
	            <h4 class="media-heading">START GAMMU.bat<small> &nbsp;<i>C:\Users\Administrator\Desktop</i></small></h4>
	            <p>Jalankan file tersebut maka service gammu (SMS Gateway) akan menyala.</p>
	        </div>
	    </div>
	    <br>
	    <br>
	    <hr />
	    <div class="media">
	        <a href="#" class="pull-left">
	            <img src="stop.jpg"  width="100%" height="100%" class="media-object" alt="Sample Image">
	        </a>
	        <div class="media-body">
	            <h4 class="media-heading">STOP GAMMU.bat<small> &nbsp;<i>C:\Users\Administrator\Desktop</i></small></h4>
	            <p>Jalankan file tersebut maka service gammu (SMS Gateway) akan berhenti.</p>
	        </div>
	    </div>
	    <br>
	    <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@stop

