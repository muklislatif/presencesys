@section('pagehead')
<h1>
	Data Kelas
	<small>Edit Data Kelas</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{url('class')}}">Kelas</a></li>
	<li><a>Edit Kelas</a></li>
</ol>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements disabled -->
		<div class="box box-primary">

			{{ Form::open(array('url' => url("class/$class->id"), 'method' => 'put', 'role' => 'form', 'data-parsley-validate')) }}
			<div class="box-body">
					<!-- text input -->
					<div class="form-group"> 
						<label>Nama Kelas</label>
						<input type="text" class="form-control" name="name" required value="{{$class->name}}">
					</div>

					<!-- textarea -->
					<div class="form-group">
						<label>Catatan untuk kelas</label>
						<textarea class="form-control" name="note" rows="3" placeholder="Catatan jika diperlukan">{{$class->note}}</textarea>
					</div>
			</div><!-- /.box-body -->

			<div class="box-body table-responsive no-padding">
				<h4>Daftar murid dalam kelas</h4>
				<table class="table table-hover">
					<tbody>
						<tr>
							<th>#</th>
							<th>NIS</th>
							<th>Nama Murid</th>
							<th></th>
						</tr>
						@foreach($students as $key=>$list)
						<tr>
							<td>{{$key+1}}</td>
							<td>{{$list->NIS}}</td>
							<td>{{$list->name}}</td>
							<td>
								{{ Form::open(array('url' => url("class-destroy/$list->id"), 'role' => 'form')) }}
								<button class="btn btn-danger btn-sm" ><i class="fa fa-scissors"></i> Hapus</button>
								{{Form::close()}}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div><!-- /.box-body -->

			<div class="box-body">
					<!-- text input -->
				<div class="form-group"> 
					<label>Tambah murid kedalam kelas</label>
					<select name="studentadd[]" class="chosen-select form-control" multiple>
						@foreach($addstudents as $key=>$list)
						<option value="{{$list->id}}">{{$list->name}}</option>
						@endforeach
					</select>
				</div>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.box -->
	</div>
</div>

@stop