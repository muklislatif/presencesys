@section('pagehead')

<style type="text/css">
         body,div.box,div.row,div.col-xs-12,div.content
         {
            background-color: #255232;
            background-image: url(assets/lte/img/blur-background09.jpg);
            background-position: top left;
            background-attachment: fixed;
         }
</style>
<h1 align="left" style="margin-top:40px;">
	  
	
</h1>
<h1 align="center" style="margin-top:40px;">
	  <img src="{{url('assets/lte')}}/img/logo215.png"  align="left"width="8%" alt="User Image" style="margin-left:40px;margin-top:50px;" /> Rekapitulasi Kehadiran - {{$array_hari[date('N')],', ',date ('j'),' ',$array_bulan[date('n')],' ',date('Y')}} <img src="{{url('assets/lte')}}/img/btplogo.png" style="margin-right:40px;margin-top:37px;" align="right" width="8%" alt="User Image" />
	
</h1>
<ol align="center" class="breadcrumb">
	<!--<li><font size="4"><a href="rekap-broadcast2">Next</a></font></li>-->
	<li ><font size="5">Jumlah Siswa Hadir:<strong ><font size="6"> {{$jumsemua}} (<?php echo number_format($persensemua,0,",",",")?>%)</strong> </font></li>
	
</ol>



<!--<div>
	  <center><img src="{{url('assets/lte')}}/img/btplogo.png" style="position:abolute; " align="right" width="30%" alt="User Image" /></center>
</div>-->
<div class="row" >
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-tools">
					
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover"><br>
					<tbody>
						<!--<tr>
							<th>#</th>
							<th>Nama Kelas</th>
							<th></th>
						</tr>-->
						@if(count($kelasxi) > 0)
						@foreach($kelasxi as $key => $value)
						<tr>
							<div class="col-lg-3 col-xs-6">
								@if(str_contains($kelasxi[$key]['name'],'XI TKJ'))
									<div class="small-box bg-olive">
								
								@elseif(str_contains($kelasxi[$key]['name'],'XI MM'))
									<div class="small-box bg-navy">
								
								@elseif(str_contains($kelasxi[$key]['name'],'XI TJA'))
									<div class="small-box bg-orange">
								@endif
									<div class="inner">
										<h3>
											{{$kelasxi[$key]['name']}}
										</h3>
										<h4>
											Hadir : <strong ><font size="6">{{$jumhadir[$value->id] }} (<?php echo number_format($persenhadir[$value->id],0,",",",")?>%) </font></strong>
										</h4>
										<h4>
											Terlambat : {{$jumtelat[$value->id] }}
										</h4>
										<h4>
											Tidak hadir : {{$jumtdkhadir[$value->id] }}
										</h4>
									</div>
									<div class="icon">
										
										
										@if(str_contains($kelasxi[$key]['name'],'XI TKJ 1'))
											<img src="{{url('assets/lte')}}/img/walikelas/xi-tkj-1.jpg" style="margin-bottom:9px" width="30%" align="right" class="img-circle" alt="User Image" />
										@elseif(str_contains($kelasxi[$key]['name'],'XI TKJ 2'))
											<img src="{{url('assets/lte')}}/img/walikelas/xi-tkj-2.jpg"style="margin-bottom:9px" width="30%" align="right"class="img-circle" alt="User Image" />
										@elseif(str_contains($kelasxi[$key]['name'],'XI MM 1'))
											<img src="{{url('assets/lte')}}/img/walikelas/xi-mm-1.jpg" style="margin-bottom:9px" width="30%" align="right"class="img-circle" alt="User Image" />
										@elseif(str_contains($kelasxi[$key]['name'],'XI MM 2'))
											<img src="{{url('assets/lte')}}/img/walikelas/xi-mm-2.jpg"style="margin-bottom:9px" width="30%"align="right" class="img-circle" alt="User Image" />
										@elseif(str_contains($kelasxi[$key]['name'],'XI TJA 1'))
											<img src="{{url('assets/lte')}}/img/walikelas/xi-tja-1.jpg" style="margin-bottom:9px"width="30%" align="right"class="img-circle" alt="User Image" />
										@elseif(str_contains($kelasxi[$key]['name'],'XI TJA 2'))
											<img src="{{url('assets/lte')}}/img/walikelas/xi-tja-2.jpg"style="margin-bottom:9px" width="30%"align="right" class="img-circle" alt="User Image" />
										@endif
									</div>
									</div>
								</div>
							</a>
						</div>
						</tr>
						
						@endforeach
						@else
							<center><h4>data presensi kosong</h4></center>
						@endif
					</tbody>
				</table>
			</div><!-- /.box-body -->
			
		</div><!-- /.box -->
	</div>
</div>
@stop 