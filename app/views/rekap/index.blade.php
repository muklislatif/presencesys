@section('pagehead')
<h1>
	Daftar Rekapitulasi
	<small>Daftar rekapitulasi presensi murid</small>
</h1>
<ol class="breadcrumb">
	<li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a>Kelas</a></li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-tools">
					{{ Form::open(array('url' => url("class-search"), 'role' => 'form')) }}
					<div class="input-group">
						<a href='{{url("rekap-broadcast")}}' target="_blank" class="btn btn-sm btn-primary" style="color:white;"><strong>Dashboard</strong></a>
						<input type="text" value="{{isset($keyword)?$keyword:''}}" name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search">
						<div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						</div>
					</div>
					{{ Form::close() }}
				</div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tbody>
						<tr>
							<th>#</th>
							<th>Nama Kelas</th>
							<th></th>
						</tr>
						@foreach($class as $key=>$list)
						<tr>
							<td>{{$key+1}}</td>
							<td>{{$list->name}}</td>
							<td >
								{{ Form::open(array('url' => url("class-hapus/$list->id"))) }}
								<!--<a class="btn btn-info btn-sm" href='{{url("class/$list->id/edit")}}'><i class="fa fa-edit"></i> Edit</a>-->
								<a class="btn btn-primary btn-sm" href='{{url("tap/$list->id")}}'><i class="fa fa-users"></i> Presensi</a>
								<!--<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Hapus kelas?')"> <i class="fa fa-fw fa-scissors"></i>  Delete</button>-->
								{{ Form::close() }}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				{{$class->links()}}
			</div>
		</div><!-- /.box -->
	</div>
</div>
@stop