<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//api
Route::resource('/apisocket', 'apiSocketController');
Route::resource('/apisms', 'smsController');
Route::resource('/apimachine', 'apiMachineController');



//
Route::get('/test',function(){
	$cardId = 1;
	return $cardUser = CardUsers::where('idCard', "$cardId")->first();
	if(empty($cardUser)){
		return array('message' => 'm003:cardUser not registered'.$cardId.'hallo');
	}
});

//
Route::get('/testsms',function(){
	
	$stat=sendSms("081220812295", "hallo");
	echo $stat;
});

Route::group(array('before'=>'logged'), function()
{
	Route::resource('/dashboard', 'dashboardController');
	Route::resource('/admin', 'userController');
	Route::get('/logout', 'loginController@destroy');
	Route::get('/admin-search', 'userController@search');
	Route::post('/admin-search', 'userController@search');
	Route::resource('/card', 'cardController');
	Route::get('/card-search', 'cardController@search');
	Route::post('/card-search', 'cardController@search');
	Route::resource('/student', 'cardUserController');
	Route::get('/student-search', 'cardUserController@search');
	Route::post('/student-search', 'cardUserController@search');
	Route::resource('/student-full', 'allCardUserController');
	Route::get('/student-full-search', 'allCardUserController@search');
	Route::post('/student-full-search', 'allCardUserController@search');
	Route::get('/student-full-filter', 'allCardUserController@filtering');
	Route::post('/student-full-filter', 'allCardUserController@filtering');
	Route::resource('/class', 'groupController');
	Route::resource('/rekap', 'rekapController');

	Route::get('/class-search', 'groupController@search');
	Route::post('/class-search', 'groupController@search');
	Route::get('/class-update/{id}', 'groupController@update');
	Route::post('/class-update/{id}', 'groupController@update');
	Route::get('/class-remove', 'groupController@remove');
	Route::post('/class-remove', 'groupController@remove');
	Route::post('/class-hapus/{id}', 'groupController@hapus');
	Route::resource('/machine', 'machineController');
	Route::get('/machine-search', 'machineController@search');
	Route::post('/machine-search', 'machineController@search');
	Route::resource('/tapsch', 'tapSchController');
	Route::resource('/tapsch-add', 'tapSchController@add');
	Route::get('/tapsch-type', 'tapSchController@type');
	Route::post('/tapsch-type-remove', 'tapSchController@remove_type');
	Route::get('/tapsch-month', 'MonthTapSchController@create');
	Route::post('/tapsch-month', 'MonthTapSchController@store');
	Route::get('/tapsch-search', 'tapSchController@search');
	Route::post('/tapsch-search', 'tapSchController@search');
	Route::get('/tapsch-filter', 'tapSchController@filtering');
	Route::post('/tapsch-filter', 'tapSchController@filtering');
	Route::resource('/tap', 'tapController');
	Route::post('/tap-export/{id}', 'tapController@export');
	Route::get('/tap-search/{id}', 'tapController@search');
	Route::post('/tap-search/{id}', 'tapController@search');
	Route::resource('/parent', 'notifController');
	Route::resource('/parent-broadcast', 'notifController@broadcast');
	Route::resource('/parent-send', 'notifController@send');
	Route::resource('/parent-outbox', 'notifController@outbox');
	Route::resource('/parent-pulsa', 'notifController@pulsa');
	Route::post('/parent-search', 'notifController@search');
	Route::get('/parent-search', 'notifController@search');
	Route::resource('/message', 'notifMessController');
	Route::resource('/setting', 'settingController');
});

// templating

Route::group(array('before'=>'nlogged'), function()
{
	Route::resource('/', 'loginController');
	Route::resource('/login', 'loginController');

	Route::resource('/rekap-broadcast', 'dashboardController@broadcast');
	Route::resource('/rekap-broadcastxi', 'dashboardController@broadcastxi');
	Route::resource('/rekap-broadcastxii', 'dashboardController@broadcastxii');
	Route::resource('/rekap-broadcast2', 'dashboardController@kehadiran');
	Route::resource('/rekap-broadcast2xi', 'dashboardController@kehadiranxi');
	Route::resource('/rekap-broadcast2xii', 'dashboardController@kehadiranxii');
	Route::resource('/maintenance', 'dashboardController@maintenance');
});
