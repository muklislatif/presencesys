<?php

class rekapController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// return $this->akun;
		$class = Group::paginate(10);
		$this->layout->content = View::make('rekap.index')->with('class', $class);
		
		
	/*	$file = fopen("C:/Users/Administrator/Desktop/11TJA2.txt", "r");
		if ($file) {
		    while (! feof($file)) {
		        //echo fgets($file). "<br />";	
		        $nis = fgets($file);
		        $delsch = "update cardusers set idClass=13 where NIS = '$nis'";
		        $tapsch = DB::select(DB::raw($delsch));

		    }

		    fclose($file);
		} else {
		    // error opening the file.
		    return 1;
		}*/ 
	}

	public function search()
	{
		$keyword = Input::get('keyword');
		$class = Group::where('name', 'like', "%$keyword%")->paginate(10);
		$this->layout->content = View::make('rekap.index')->with('class', $class)->with('keyword', $keyword);
	}

	
	public function broadcast()
	{
		  


		$nowTime = date('Y-m-d');
		$idjadwal = getIdTapSch($nowTime);
		$idclass = getIdClass($nowTime);
		
		$c = array(); 
		for($i=0; $i <= count($idclass)-1; $i++){
		  $a[$i] = $i;
		  $class = Group::select('name')->where('id', $idclass[$i]['idClass'])->get();
		  if(count($class)==0){
		  		$c[$i] = 'nihil';
		  }else{
		  		$c[$i] = $class;
		  }
		} 
		//return $c[2][0]['name'];

		$a = array(); 
		
		$b = array(); 
		$k = 1;
		for($i=0; $i <= count($idjadwal)-1; $i++){
		  $a[$i] = $i;
		  $taps = Taps::where('idTapSch', $idjadwal[$i]['id'] )->where('status', 1)->get();
		  if(count($taps)==0){
		  		$b[$i] = 'kosong';
		  }else{
		  		$b[$i] = count($taps);
		  }
		}
		 // return $b;
		$url1=$_SERVER['REQUEST_URI'];

		header("Refresh: 2; URL=$url1");
		//return $b[1];   
		$this->layout->content = View::make('rekap.broadcast')->with('class', $c)->with('taps', $b);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('rekap.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
						'name' => 'required',
						'note'	=> 'between:1,255'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to('class/create')->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$group = new Group;

			$group->name = Input::get('name');
			$group->note = Input::get('note');

			if ($group->save()) 
			{
				return Redirect::to('/rekap')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$class = Group::find($id);
		$students = CardUsers::where('idClass', $id)->get();
		$addstudents = CardUsers::where('idClass', 0)->get();
		$this->layout->content = View::make('group.edit')->with('class', $class)->with('students', $students)->with('addstudents', $addstudents);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
						'name' => 'required',
						'note'	=> 'between:1,255'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to("class/$id/edit")->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$class = Group::find($id);

			$class->name = Input::get('name');
			$class->note = Input::get('note');

			if ($class->save()) 
			{
				$addstudent = Input::get('studentadd');

				if (!is_null($addstudent)) 
				{
					DB::table('cardUsers')
			            ->whereIn('id', $addstudent)
			            ->update(array('idClass' => $id));
					return Redirect::to('/class')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
				}
				
			}
			

			else 
			{
				return 'feyil';
			}

		}

	//	return Input::get('studentadd');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	
		//$class = Group::find($id);
		/*$students = CardUsers::find($id);
		$students->idClass = 0;
		if ($students->save()) 
		{
			return Redirect::to('class');
		}*/
		$delsch = "update cardusers set idClass=0 where id = $id";
		$tapsch = DB::select(DB::raw($delsch));
				
		return Redirect::to('rekap');


	}

	public function hapus($id)
	{
	
		//$class = Group::find($id);
		/*$students = CardUsers::find($id);
		$students->idClass = 0;
		if ($students->save()) 
		{
			return Redirect::to('class');
		}*/
		$delsch = "delete from class where id = $id";
		$a = "update cardusers set idClass=0 where idClass = $id";
		$b = DB::select(DB::raw($a));
		$tapsch = DB::select(DB::raw($delsch));

		return Redirect::to('rekap');


	}


}
