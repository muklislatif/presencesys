<?php

class apiMachineController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		// get post item
		// get outomatic client host
		$ip = $_SERVER['REMOTE_ADDR'];
		$cardId = (Input::get('data'));
		$cardId = preg_replace("/[^A-Za-z0-9 ]/", '', $cardId);
		if (empty($ip) || empty($cardId)){
			return array('message' => 'm009:you are not sumbit complete data');
		}
		

		$machine = Machines::where('ip',$ip)->first();
		if(empty($machine)){
			return array('message' => 'm001:mahines not authorised');

		}

		// check card
		$card = Cards::where('cardNum',$cardId)->first();
		if(empty($card)){
			return array('message' => 'm002:card not registered');
		}

		// check active card
		$card = Cards::where('cardNum',$cardId)->where('status',1)->first();
		if(empty($card)){
			return array('message' => 'm010:card not active');
		}

		$cardUser = CardUsers::where('idCard', $card->id)->first();
		if(empty($cardUser)){
			return array('message' => 'm003:cardUser not registered');
		}


		/*
		$todayDate = date('Y-m-d'); 
		$tapSch = TapSch::where('date',$todayDate)->get();
		if(empty($cardUser)){
			return array('message' => 'm004:tapping schedule is not available for today');
		}
		*/

		//$nowTime = date('Y-m-d H:m:s');

		$tapSch = TapSch::whereRaw('now() between startTapTime and finishTapTime')->where('idClass',$cardUser->idClass)->first();
		if(empty($tapSch)){
			return array('message' => 'm005:tapping schedule is not available for now');
		} else {

			// check user, is registered in tapping schedule
			$tap = Taps::where('idTapSch',$tapSch->id)->where('idCardUser',$cardUser->id)->first();
			if (empty($tap)){
				return array('message' => 'm006:your id is not found in tapping schedule');
			} else {

				// check if alredy tapping
				if ($tap->status == 1){
					return array('message' => 'm008:you already tapping');
				}
			}

			// update tapiing from 0 to 1
			$updateData = array('status' => 1);
			$updateStatus = Taps::where('idTapSch',$tapSch->id)->where('idCardUser',$cardUser->id)->update($updateData);
			if($updateStatus=1){
				return array('message' => 'm007: '.date('H:m:s')." ".$cardUser->name);
			} 
		}











		return array('message' => 'm011:something happens');

		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
