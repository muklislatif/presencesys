<?php

class tapSchController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$schedule = TapSch::orderBy('date', 'desc')->paginate(10);
		$kelas = Group::paginate(100);
		
		$this->layout->content = View::make('tapSch.index')->with('schedule', $schedule)->with('kelas',$kelas);
	}

	public function search()
	{
		$keyword = Input::get('keyword');
		$kelas = Group::paginate(100);
		if ($keyword=='') 
		{
			return Redirect::to('tapsch');
		}

		$schedule = DB::table('tapSch')
		            ->orWhere('tapSch.type', 'like', "%$keyword%")
		            ->orWhere('tapSch.date', 'like', "%$keyword%")
		            ->orWhere('tapSch.startTapTime', 'like', "%$keyword%")
		            ->orWhere('tapSch.finishTapTime', 'like', "%$keyword%")
		            ->orderBy('date', 'desc')
		            ->paginate(3000);

		//$schedule = TapSch::where('')->paginate(10);
		$this->layout->content = View::make('tapSch.index')->with('schedule', $schedule)->with('keyword', $keyword)->with('kelas',$kelas);
	}

	public function filtering()
	{
		$filter = Input::get('filter');
		$kelas = Group::paginate(10);
		if ($filter=='') 
		{
			return Redirect::to('tapsch');
		}

		$schedule = DB::table('tapSch')
		            ->join('class', 'class.id', '=', 'tapSch.idClass')
		            ->select('tapSch.id', 'tapSch.type', 'class.name', 'tapSch.date', 'tapSch.finishTapTime','tapSch.startTapTime')
		            ->where('class.name', 'like', "%$filter%")
		            ->paginate(1000);

		//$schedule = TapSch::where('')->paginate(10);
		$this->layout->content = View::make('tapSch.index')->with('schedule', $schedule)->with('filter', $filter)->with('kelas',$kelas);	
	}

	public function type(){ 
		$type = DB::table('type')->get();
		$this->layout->content = View::make('tapSch.type')->with('type', $type);	
	}

	public function remove_type(){ 

		$id = Input::get('id');
		$alamat = 'tapSch-type';

		if($id[0] === 'all'){
			$q = "update cardusers set name=''";
			$run = DB::select(DB::raw($q));
			return Redirect::to($alamat);
			
		}else{
			foreach ($id as $key => $value) {
				$delsch = "update cardusers set name='' where id = $value";
				$tapsch = DB::select(DB::raw($delsch));
			}
			return Redirect::to($alamat);
		}
	}

	public function add() { 
		$input = Input::get('tipe');
		if( $input !== null){
			$data = array('name' => Input::get('tipe'));
			$jenis = DB::table('type')->insert($data);
		}else{
			return 'no data input';
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$class = Group::where('deleted_at', null)->all();
		$this->layout->content = View::make('tapSch.create')->with('class', $class);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
						'date' => 'required',
						'tipe' => 'required',
						'intervalbawah' => 'required',
						'intervalatas' => 'required',
						'class' => 'required',
						'note'	=> 'between:1,255'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to("class/$id/edit")->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$bawah = Input::get('date')." ".Input::get('intervalbawah').":00";
			$atas = Input::get('date')." ".Input::get('intervalatas').":00";

			if (strtotime($bawah)<strtotime(date("Y-m-d H:i:s"))) 
			{
				return Redirect::to("class/create")->with('message', 'Jam mulai tidak boleh kurang dari sekarang')->with('type', 2)->withInput();
			}

			if (strtotime($atas)<strtotime($bawah)) 
			{
				return Redirect::to("class/create")->with('message', 'Jam selesai harus lebih dari jam mulai')->with('type', 2)->withInput();
			}

			$bentrok = TapSch::whereRaw(DB::raw("(TIMESTAMP('$bawah') between startTapTime AND finishTapTime) OR (TIMESTAMP('$atas') between startTapTime AND finishTapTime)"))->where('idClass', Input::get('class'))->count();

			if ($bentrok==10) 
			{
				$schedule = TapSch;

				$schedule->date = Input::get('date');
				$schedule->type = Input::get('tipe');
				$schedule->startTapTime = $bawah;
				$schedule->finishTapTime = $atas;
				$schedule->idClass = Input::get('class');
				$schedule->note = Input::get('note');

				if ($schedule->save()) 
				{
					$students = CardUsers::where('idClass', Input::get('class'))->lists('id');

					$idsch = $schedule->id;

					if (!empty($students)) 
					{
						$insert = array();

						foreach ($students as $key=>$value) 
						{
							$data = array('idTapSch' => $idsch,
											'idCardUser' => $value,
											'status' => 0
											 );

							$insert[] = $data;
						}

						//return $insert;
						DB::table('taps')->insert($insert);
					}

					//return Redirect::to('/tapsch')->with('message', 'Kategori telah ditambahkan')->with('type', 1);	
				}
				else 
				{
					return 'feyil';
				}
			}
			else 
			{
				return Redirect::to("tapsch")->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$schedule = TapSch::find($id);
		$class = Group::all();
		$this->layout->content = View::make('tapSch.edit')->with('schedule', $schedule)->with('class', $class);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
						'date' => 'required',
						'tipe' => 'required',
						'intervalbawah' => 'required',
						'intervalatas' => 'required',
						'class' => 'required',
						'note'	=> 'between:1,255'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to("class/$id/edit")->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$bawah = Input::get('date')." ".Input::get('intervalbawah').":00";
			$atas = Input::get('date')." ".Input::get('intervalatas').":00";

			if (strtotime($bawah)<strtotime(date("Y-m-d H:i:s"))) 
			{
				return Redirect::to("class/$id/edit")->with('message', 'Jam mulai tidak boleh kurang dari sekarang')->with('type', 2)->withInput();
			}

			if (strtotime($atas)<strtotime($bawah)) 
			{
				return Redirect::to("class/$id/edit")->with('message', 'Jam selesai harus lebih dari jam mulai')->with('type', 2)->withInput();
			}

			$schedule = TapSch::find($id);

			$schedule->date = Input::get('date');
			$schedule->type = Input::get('tipe');
			$schedule->startTapTime = date("Y-m-d")." ".Input::get('intervalbawah').":00";
			$schedule->finishTapTime = date("Y-m-d")." ".Input::get('intervalatas').":00";
			$schedule->idClass = Input::get('class');
			$schedule->note = Input::get('note');

			if ($schedule->save()) 
			{
				return Redirect::to('/tapsch')->with('message', 'Kategori telah ditambahkan')->with('type', 1);	
			}
			else 
			{
				return 'feyil';
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$tapsch = TapSch::find($id);
		$delsch = "delete from tapsch where id = $id";
		$tapsch = DB::select(DB::raw($delsch));
		$kueri = "delete from taps where idTapSch = $id";
		$cardusers = DB::select(DB::raw($kueri));

		return Redirect::to('tapsch');
		
	}


}
