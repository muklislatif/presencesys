<?php

class machineController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$machine = Machines::paginate(10);
		$this->layout->content = View::make('machine.index')->with('machine', $machine);
	}

	public function search()
	{
		$keyword = Input::get('keyword');
		$machine = Machines::where('ip', 'like', "%$keyword%")->paginate(10);
		$this->layout->content = View::make('machine.index')->with('machine', $machine)->with('keyword', $keyword);	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('machine.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
						'ip' => 'required',
						'note'	=> 'between:1,255'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to('machine/create')->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$machine = new Machines;

			$machine->ip = Input::get('ip');
			$machine->note = Input::get('note');

			if ($machine->save()) 
			{
				return Redirect::to('/machine')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$machine = Machines::find($id);
		$this->layout->content = View::make('machine.edit')->with('machine', $machine);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
						'ip' => 'required',
						'note'	=> 'between:1,255'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to("machine/$id/edit")->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$machine = Machines::find($id);

			$machine->ip = Input::get('ip');
			$machine->note = Input::get('note');

			if ($machine->save()) 
			{
				return Redirect::to('/machine')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$machine = Machines::find($id);

		if ($machine->delete()) 
		{
			return Redirect::to('machine');
		}
	}


}
