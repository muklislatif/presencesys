<?php

class allCardUserController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		
		
		$akun = User::find(Sentry::getUser()->id);
		
			$student = CardUsers::join('cards', 'CardUsers.idCard', '=', 'cards.id')
					->select('cardusers.id','cardusers.idClass','cardusers.idNotif','cardusers.name'
						,'cardusers.NIS','cardusers.address','cardusers.note','cardusers.updated_at'
						,'cardusers.created_at','cardusers.deleted_at','cards.cardNum','cards.status')
					->paginate(10);
			$kelas = Group::paginate(100);
		
		$this->layout->content = View::make('allCardUser.index')->with('student', $student)->with('kelas',$kelas)->with('akun', $akun);
	}

	public function search()
	{
		$keyword = Input::get('keyword');
		$kelas = Group::paginate(100);
		if ($keyword=='') 
		{
			return Redirect::to('student-full');
		}
		

		$akun = User::find(Sentry::getUser()->id);

		
			$student = CardUsers::join('cards', 'CardUsers.idCard', '=', 'cards.id')
					->select('cardusers.id','cardusers.idClass','cardusers.idNotif','cardusers.name'
						,'cardusers.NIS','cardusers.address','cardusers.note','cardusers.updated_at'
						,'cardusers.created_at','cardusers.deleted_at','cards.cardNum','cards.status')
					->where('name', 'like', "%$keyword%")
					->orWhere('NIS', 'like', "%$keyword%")
					
					->orWhere('address', 'like', "%$keyword%")
					->orWhere('cards.cardNum', 'like', "%$keyword%")
					->paginate(100);
			$kelas = Group::paginate(100);
		
		

		$this->layout->content = View::make('allCardUser.index')->with('student', $student)->with('keyword', $keyword)->with('kelas', $kelas)->with('akun', $akun);
	}

	public function filtering()
	{
		$filter = Input::get('filter');
		 
		$getid = Group::where('name',$filter )->lists('id');
		$kelas = Group::paginate(100);
		if ($filter=='') 
		{
			return Redirect::to('student-full');
		}

		 $student = CardUsers::join('cards', 'CardUsers.idCard', '=', 'cards.id')
					->select('cardusers.id','cardusers.idClass','cardusers.idNotif','cardusers.name'
						,'cardusers.NIS','cardusers.address','cardusers.note','cardusers.updated_at'
						,'cardusers.created_at','cardusers.deleted_at','cards.cardNum','cards.status')
					->where('cardusers.idClass', '=', "$getid[0]")
					->paginate(100);         
		$akun = User::find(Sentry::getUser()->id);
		$this->layout->content = View::make('allCardUser.index')->with('student', $student)->with('filter', $filter)->with('kelas', $kelas)->with('akun', $akun);	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$kueri = "select * from (select cards.id, cards.deleted_at, cards.cardNum, cardUsers.name from cards left join cardUsers on cardUsers.idCard=cards.id) t1 where t1.name is null and t1.deleted_at is null;";

		$cards = DB::select(DB::raw($kueri));

		$kueri = "select * from (select notif.id, notif.email, notif.name as parent, cardUsers.name as student from notif left join cardUsers on cardUsers.idNotif=notif.id) t1 where t1.student is null;";

		$notif = DB::select(DB::raw($kueri));

		$kelas = Group::paginate(100);

		$akun = User::find(Sentry::getUser()->id);
		$this->layout->content = View::make('allCardUser.create')->with('cards', $cards)->with('class', $kelas)->with('notif',$notif)->with('akun', $akun);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//$kueri = "select * from (select cards.id, cards.deleted_at, cards.cardNum, cardUsers.name from cards left join cardUsers on cardUsers.idCard=cards.id) t1 where t1.name is null and t1.deleted_at is null;";
		//$cards = DB::select(DB::raw($kueri));

		
			$rules = array(
							'name' => 'required',
							'nis'	=> 'required',
							'cardnum' => 'required',
							'noteCard'	=> 'between:1,255',
							'status' => 'required',
							'class'	=> 'required',
							'address' => 'required',
							'email' => 'required|email',
							'phone' => 'required|numeric'
							//'noteNotif'	=> ''
					      );
			$validation = Validator::make(Input::all(),$rules);

	        if ($validation->fails())
			{
				return Redirect::to('student-full/create')->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
			    //return $messages = $validation->messages();
			}
			else
			{
				
				$cards = new Cards;
				$cards->cardNum = Input::get('cardnum');
				$cards->status = Input::get('status');
				$cards->note = Input::get('noteCard');

				if ($cards->save()) 
				{
					$notif = new Notif;

					$notif->name = Input::get('name');
					$notif->email = Input::get('email');
					$notif->phone = Input::get('phone');
					$notif->note = Input::get('note');

					if ($notif->save()) 
					{
						$student = new CardUsers;

						$student->name = Input::get('name');
						$student->NIS = Input::get('nis');
						$student->idCard = $cards->id;
						$student->idNotif = $notif->id;
						$student->idClass = Input::get('class');
						$student->address = Input::get('address');	

						if ($student->save()) 
						{						
							return Redirect::to('student-full')->with('message', 'Kategori telah ditambahkan')->with('type',1);
						}
						else 
						{
							return 'feyil';
						}
					}
					else 
					{
						return 'feyil';
					}
				}
				else 
				{
					return 'feyil';
				}

			}
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$student = CardUsers::find($id);

		$kueri = "select * from (select cards.id, cards.cardNum, cardUsers.id as kartu from cards left join cardUsers on cardUsers.idCard=cards.id) t1 where t1.kartu is null OR t1.kartu=$student->id;";

		$cards = DB::select(DB::raw($kueri));

		$kueri = "select * from (select notif.id, notif.email, notif.name as parent, cardUsers.name as student from notif left join cardUsers on cardUsers.idNotif=notif.id) t1 where t1.student is null;";

		$notif = DB::select(DB::raw($kueri));

		$akun = User::find(Sentry::getUser()->id);
		$this->layout->content = View::make('allCardUser.edit')->with('cards', $cards)->with('student', $student)->with('notif', $notif)->with('akun', $akun);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
						'name' => 'required',
						'nis'	=> 'required',
						'card'	=> 'required',
						'idCard'	=> 'required',
						'notif'	=> 'required',
						'idnotif'	=> 'required',
						'address' => 'required'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			//return Redirect::to("student/$id/edit")->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    return $messages = $validation->messages();
		}
		else
		{
			$idcardtemp = CardUsers::select('idCard')->where('idCard', Input::get('idCard'))->get();
			
			
			$kartu = Cards::find($idcardtemp[0]['idCard']);
			$kartu->cardNum = Input::get('card');

			$student = CardUsers::find($id);
			$student->name = Input::get('name');
			$student->NIS = Input::get('nis');

			$phone = Notif::find(Input::get('idnotif'));
			$phone->phone = Input::get('notif');


			//$student->idNotif = Input::get('parent');
			$student->address = Input::get('address');

			if ($student->save() && $kartu->save() && $phone->save()) 
			{
				return Redirect::to('/student-full')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}

		}
	}


	/**2
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$card =  CardUsers::find($id);
		
		/*if ($card->delete()) 
		{
			return Redirect::to('student-full');
		}*/
		$delsch = "delete from cardusers where id = $id";
		$tapsch = DB::select(DB::raw($delsch));

		return Redirect::to('student-full');
	}


}
