<?php

class MonthTapSchController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$class = Group::all();
		$typesch = DB::table('type')->get();
		$this->layout->content = View::make('tapSch.createmonth')->with('class', $class)->with('typesch',$typesch);
	}
	

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//return Input::get('kelas');

		$req = 'required';
		if(Input::get('kelas')== 1){
			$req = '';
		}

		$rules = array(
						'rangedate' => 'required',
						'tipe' => $req,
						'intervalbawah' => $req,
						'intervalatas' => $req,
						'kelas' => 'required',
						'note'	=> 'between:1,255'
				      );

        $validation = Validator::make(Input::all(),$rules);


        /*$range = Input::get('rangedate');
			// Start date
			$dates = explode(" - ", $range);

			$date = $dates[0];
			 // End date
			$end_date = $dates[1];

        $datetimes = DateTime::createFromFormat('Y-m-d', $date);
		$haris = $datetimes->format('D');

		if($haris != ' ' && $haris != 'Sat'){
			return 'a';
		}else{
			return 'salah';
		}*/

        if ($validation->fails())
		{
			return Redirect::to("tapsch-month")->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$schdules = array();
			$range = Input::get('rangedate');
			// Start date
			$dates = explode(" - ", $range);

			$date = $dates[0];
			 // End date
			$end_date = $dates[1];
			
			if($date != $end_date)	{ 
				while (strtotime($date) <= strtotime($end_date)) 
				{	
					if(Input::get('kelas')== '0'){
						$bawah = $date." ".Input::get('intervalbawah').":00";
						$atas = $date." ".Input::get('intervalatas').":00";
					}else{
						$bawah = $date." 01:00:00";
						$atas = $date." 23:00:00";
					}
					
					if (strtotime($bawah)<strtotime(date("Y-m-d H:i:s"))) 
					{
						return Redirect::to("tapsch-month")->with('message', 'Jam mulai tidak boleh kurang dari sekarang')->with('type', 2)->withInput();
					}

					if (strtotime($atas)<strtotime($bawah)) 
					{
						return Redirect::to("tapsch-month")->with('message', 'Jam selesai harus lebih dari jam mulai')->with('type', 2)->withInput();
					}

					$bentrok = TapSch::whereRaw(DB::raw(" (TIMESTAMP('$bawah') between startTapTime AND finishTapTime) OR (TIMESTAMP('$atas') between startTapTime AND finishTapTime)"))->get();	
					$kls = Input::get('kelas');
					

					if(count($bentrok) == 0){
						 $bentrok = 0;
					}else{
						//return 'a';
						if($kls == '0'){
							if($bentrok[0]['idClass'] == 0){
								$bentrok = 1;
							}else{
								$bentrok = 0;
							}
						}else{
							if($bentrok[0]['idClass'] == 1){
								$bentrok = 0;
							}else{
								$bentrok = 1;
							}
						}
					}
					
					//return $bentrok;

					if ($bentrok==0) 
					{
						$datetime = DateTime::createFromFormat('Y-m-d', $date);
						$hari = $datetime->format('D');


						if($hari != 'Sun' && $hari != 'Sun'){

							$temp = array('date' => $date,
							 				'type' => Input::get('tipe'),
							 				'startTapTime' => $bawah,
							 				'finishTapTime' => $atas,
							 				'idClass' => Input::get('kelas'),
							 				'note' => Input::get('note')
											);
							//$schdules[] = $temp;
							DB::table('tapSch')->insert($temp);
							
							
						}else{
							$k = 'k';
						}
						$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
					}else 
					{
						return Redirect::to("tapsch-month")->with('message', 'JADWAL BENTROK')->with('type', 2)->withInput();
					}
				}
			}else{
					if(Input::get('kelas')== '0'){
						$bawah = $date." ".Input::get('intervalbawah').":00";
						$atas = $date." ".Input::get('intervalatas').":00";
					}else{
						$bawah = $date." 01:00:00";
						$atas = $date." 23:00:00";
					}
					
					if (strtotime($bawah)<strtotime(date("Y-m-d H:i:s"))) 
					{
						return Redirect::to("tapsch-month")->with('message', 'Jam mulai tidak boleh kurang dari sekarang')->with('type', 2)->withInput();
					}

					if (strtotime($atas)<strtotime($bawah)) 
					{
						return Redirect::to("tapsch-month")->with('message', 'Jam selesai harus lebih dari jam mulai')->with('type', 2)->withInput();
					}

					//$kls = Input::get('class');
					$bentrok = TapSch::whereRaw(DB::raw(" (TIMESTAMP('$bawah') between startTapTime AND finishTapTime) OR (TIMESTAMP('$atas') between startTapTime AND finishTapTime)"))->get();	
					$kls = Input::get('kelas');
					

					if(count($bentrok) == 0){
						 $bentrok = 0;
					}else{
						//return 'a';
						if($kls == '0'){
							if($bentrok[0]['idClass'] == 0){
								$bentrok = 1;
							}else{
								$bentrok = 0;
							}
						}else{
							if($bentrok[0]['idClass'] == 1){
								$bentrok = 0;
							}else{
								$bentrok = 1;
							}
						}
					}

					if ($bentrok==0) 
					{
						$datetime = DateTime::createFromFormat('Y-m-d', $date);
						$hari = $datetime->format('D');

						if($hari != 'Sun' && $hari != 'Sun'){
							$temp = array('date' => $date,
						 				'type' => Input::get('tipe'),
						 				'startTapTime' => $bawah,
						 				'finishTapTime' => $atas,
						 				//'idClass' => Input::get('class'),
						 				'note' => Input::get('note')
										);
							//$schdules[] = $temp;
							DB::table('tapSch')->insert($temp);
							
							
						}else{
							$k = 'k';
						}

						$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
					}else 
					{
						return Redirect::to("tapsch-month")->with('message', 'Data bentrok')->with('type', 2)->withInput();
					}
			}
			
			//return $schdules[0];
			//DB::table('tapSch')->insert($schdules);
			$kelas = Group::where('name', 'like', 'guru' )->lists('id');
			if ( Input::get('kelas') == 0) {
				$students = CardUsers::where('idClass', '<>' , $kelas[0])->lists('id');
			}else{
				$students = CardUsers::where('idClass', $kelas[0] )->lists('id');
			}
			//return print_r($students).'<br>';
			$sch = TapSch::where('date', $temp['date'])->where('type', $temp['type'])->where('startTapTime', $temp['startTapTime'])->where('idClass', Input::get('kelas'))->lists('id');
			 
			//$schdules = TapSch::lists('id');

			$dated = $dates[0];
			 $end_dated = $dates[1];
			$schduless = array();
			

			if($dated != $end_dated){
				while (strtotime($dated) <= strtotime($end_dated)) 
				{
					$datetimes = DateTime::createFromFormat('Y-m-d', $dated);
					$haris = $datetimes->format('D');

					if($haris != 'Sun' || $haris != 'Sun'){
						$schduless[] = TapSch::where('date',$dated)->where('type',Input::get('tipe'))->where('idClass', Input::get('kelas'))->lists('id');
						
					}else{
						$k = 'k';
					}
					$dated = date ("Y-m-d", strtotime("+1 day", strtotime($dated)));
				}
				  $to = count($schduless);
				  $schduless;

				if (!empty($schduless)) 
				{
					$insert = array();
					
					//return $schduless[0];
					for ($i=0; $i < $to; $i++) 
					{
						$a = $schduless[$i];
						if(implode(' ',$a) != ''){
							foreach ($students as $key2=>$value2) 
							{

									$data2 = array('idTapSch' => implode(' ', $a),
													'idCardUser' => $value2
													 );

									$insert2[] = $data2;
									
							}
							
						}
					}
					DB::table('taps')->insert($insert2);	
				}
			}
			if($dated == $end_dated){
				foreach ($students as $key3=>$value3) {
					$datas = array('idTapSch' => implode(' ', $sch),
									'idCardUser' => $value3
									 );

					$inserts[] = $datas;
				}
				DB::table('taps')->insert($inserts);	
			}

			return Redirect::to('/tapsch')->with('message', 'Jadwal telah ditambahkan')->with('type', 1);	
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
