<?php

class cardUserController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$student = CardUsers::paginate(10);
		$this->layout->content = View::make('cardUser.index')->with('student', $student);
	}

	public function search()
	{
		$keyword = Input::get('keyword');
		$student = CardUsers::where('name', 'like', "%$keyword%")->paginate(10);
		$this->layout = View::make('cardUser.index')->with('student', $student)->with('keyword', $keyword);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$kueri = "select * from (select cards.id, cards.cardNum, cardUsers.name from cards left join cardUsers on cardUsers.idCard=cards.id) t1 where t1.name is null;";

		$cards = DB::select(DB::raw($kueri));

		$kueri = "select * from (select notif.id, notif.email, notif.name as parent, cardUsers.name as student from notif left join cardUsers on cardUsers.idNotif=notif.id) t1 where t1.student is null;";

		$notif = DB::select(DB::raw($kueri));

		$this->layout->content = View::make('cardUser.create')->with('cards', $cards)->with('notif', $notif);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
						'name' => 'required',
						'nis'	=> 'required',
						'card'	=> 'required',
						'parent'	=> 'required',
						'address' => 'required'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to('card/create')->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$student = new CardUsers;

			$student->name = Input::get('name');
			$student->NIS = Input::get('nis');
			$student->idCard = Input::get('card');
			$student->idNotif = Input::get('parent');
			$student->address = Input::get('address');

			if ($student->save()) 
			{
				return Redirect::to('/student')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}

		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$student = CardUsers::find($id);

		$kueri = "select * from (select cards.id, cards.cardNum, cardUsers.id as kartu from cards left join cardUsers on cardUsers.idCard=cards.id) t1 where t1.kartu is null OR t1.kartu=$student->id;";

		$cards = DB::select(DB::raw($kueri));

		$kueri = "select * from (select notif.id, notif.email, notif.name as parent, cardUsers.name as student from notif left join cardUsers on cardUsers.idNotif=notif.id) t1 where t1.student is null;";

		$notif = DB::select(DB::raw($kueri));

		$this->layout->content = View::make('cardUser.edit')->with('cards', $cards)->with('student', $student)->with('notif', $notif);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
						'name' => 'required',
						'nis'	=> 'required',
						'card'	=> 'required',
						//'parent'	=> 'required',
						'address' => 'required'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			//return Redirect::to("student/$id/edit")->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    return $messages = $validation->messages();
		}
		else
		{
			$student = CardUsers::find($id);

			$student->name = Input::get('name');
			$student->NIS = Input::get('nis');
			$student->idCard = Input::get('card');
			$student->idNotif = Input::get('parent');
			$student->address = Input::get('address');

			if ($student->save()) 
			{
				return Redirect::to('/student')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}

		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$card =  CardUsers::find($id);
		
		if ($card->delete()) 
		{
			return Redirect::to('student');
		}
	}


}
