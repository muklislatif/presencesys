<?php

class notifController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$notif = Notif::paginate(10);

		$kill_pattern = '~(gammu-smsd)\.exe~i';

		$task_list = array();
		exec("tasklist 2>NUL", $task_list);
		
		$status =0;
		foreach ($task_list AS $task_line)
		{
		  if (preg_match($kill_pattern, $task_line, $out))
		  {
		  	//return "=> Detected: ".$out[1]."\n   Sending term signal!\n";
			//exec("taskkill /F /IM ".$out[1].".exe 2>NUL");
			$status = 1;
		  }

		}

		$this->layout->content = View::make('notif.index')->with('notif', $notif)->with('status', $status);
	}

	public function search()
	{
		$keyword = Input::get('keyword');
		$notif = Notif::where('name', 'like', "%$keyword%")->orWhere('email', 'like', "%$keyword%")->orWhere('phone', 'like', "%$keyword%")->paginate(10);
		$this->layout->content = View::make('notif.index')->with('notif', $notif)->with('keyword', $keyword);
	}


	public function broadcast() {

		$class = Group::get();

		$this->layout->content = View::make('notif.broadcast')->with('class', $class);
	}

	public function send() {

		$class = Input::get('class');
		$subject = Input::get('subject');
		$message = Input::get('message');

		$fullmessage = '['.$subject.'] '.$message;

		if($class == 'all'){

			$allclass = Group::lists('id');
			
			foreach ($allclass as $key => $value) {

				$reciever = CardUsers::where('idClass', $value)->lists('idNotif');
				foreach ($reciever as $key2 => $value2) {
			
					$notif = Notif::where('id',$value2)->first();
					$to = $notif->phone;
					//$name = $notif->name;
					
					//echo $to.' nama: '.$name.'<br>';
					DB::connection('sms')->table('outbox')->insert( array( 'DestinationNumber' => $to, 'TextDecoded' => $fullmessage));
				}
			}

			return Redirect::to('parent')->with('message', 'Pesan berhasil dikirim')->with('type', 1);

		}else{
			
			$reciever = CardUsers::where('idClass', $class)->lists('idNotif');
			foreach ($reciever as $key2 => $value2) {
		
				$notif = Notif::where('id',$value2)->first();
				$to = $notif->phone;
				//$name = $notif->name;
				
				//echo $to.' nama: '.$name.'<br>';
				DB::connection('sms')->table('outbox')->insert( array( 'DestinationNumber' => $to, 'TextDecoded' => $fullmessage));
			}

			return Redirect::to('parent')->with('message', 'Pesan berhasil dikirim')->with('type', 1);
		}

	}


	public function outbox() {

		$messages = DB::connection('sms')->table('outbox')->paginate(10);

		$kill_pattern = '~(gammu-smsd)\.exe~i';

		$task_list = array();
		exec("tasklist 2>NUL", $task_list);

		$status =0;
		foreach ($task_list AS $task_line)
		{
		  if (preg_match($kill_pattern, $task_line, $out))
		  {
		  	//return "=> Detected: ".$out[1]."\n   Sending term signal!\n";
			//exec("taskkill /F /IM ".$out[1].".exe 2>NUL");
			$status = 1;
		  }

		}

		/* jika status terakhir = 1, maka gammu service running
		if ($status == 1){ 
			echo "Gammu service running.."; 
		}
		// jika status terakhir = 0, maka service gammu berhenti
		elseif ($status == 0){
			echo "Gammu service stopped";
		}*/



		 $this->layout->content = View::make('notif.outbox')->with('status', $status)->with('messages', $messages);
	
	}


	public function pulsa()
	{
		//system("Taskkill /IM gammu-smsd.exe /F");
		//echo $exec;

		//system('gammu-smsd.exe -c smsdrc -u');
		$output = system('gammu -c gammurc -s 1 getussd *111*1#');
		//system('gammu-smsd.exe -c smsdrc -i');
		//$output = system("gammu getussd #111*1#");

		//$output = exec("C:\command.exe");		
		return Redirect::to('parent')->with('message', $output)->with('type', 1);
	}

	public function start()
	{

		$output = system('net start GammuSMSD');
		
		return Redirect::to('parent')->with('message', $output)->with('type', 1);
	}

	public function stop()
	{

		$output = system('net stop GammuSMSD');
			
		return Redirect::to('parent')->with('message', $output)->with('type', 1);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('notif.create');
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
						'name' => 'required',
						'email' => 'required|email',
						'phone' => 'required|numeric',
						'note'	=> ''
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to('parent/create')->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$notif = new Notif;

			$notif->name = Input::get('name');
			$notif->email = Input::get('email');
			$notif->phone = Input::get('phone');
			$notif->note = Input::get('note');

			if ($notif->save()) 
			{
				return Redirect::to('/parent')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}

		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$notif = Notif::find($id);
		$this->layout->content = View::make('notif.edit')->with('notif',$notif);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
						'name' => 'required',
						'email' => 'required|email',
						'phone' => 'required|numeric',
						'note'	=> ''
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to("parent/$id/edit")->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$notif = Notif::find($id);

			$notif->name = Input::get('name');
			$notif->email = Input::get('email');
			$notif->phone = Input::get('phone');
			$notif->note = Input::get('note');

			if ($notif->save()) 
			{
				return Redirect::to('/parent')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}

		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$notif = Notif::find($id);

		if ($notif->delete()) 
		{
			return Redirect::to('parent');
		}
	}


}
