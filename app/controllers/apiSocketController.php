<?php

class apiSocketController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$time_start = microtime(true);
		// get post item
		$ip = (Input::get('ip'));
		$cardId = (Input::get('data'));
		//$ip = '192.168.1.19';
		//$cardId = '7ba4ea90';

		$cardId = preg_replace("/[^A-Za-z0-9 ]/", '', $cardId);
		if (empty($ip) || empty($cardId)){
			$time_end = microtime(true);
			$exe = ($time_end-$time_start)*1000;
			return array('message' => 'm009:you are not sumbit complete data'.' '.number_format($exe,5));
		}
		$card = Cards::where('cardNum',$cardId)->first();
		if(empty($card)){
			$time_end = microtime(true);
			$exe = ($time_end-$time_start)*1000;
			return array('message' => 'm002:card not registered'.' '.number_format($exe,5));
		}
		$cardUser = CardUsers::where('idCard', $card->id)->first();
		$nowTime = date('Y-m-d H:i:s');
		$tapSch = TapSch::whereRaw("'$nowTime' between startTapTime and finishTapTime")->where('date',date('Y-m-d'))->first();
		$tapSchDtg = TapSch::where('type', 'datang')->where('date',date('Y-m-d'))->first();
		$tapSchPlg = TapSch::where('type', 'pulang')->where('date',date('Y-m-d'))->first();
		$tapSchLate = TapSch::whereRaw("'$nowTime' between '$tapSchDtg[finishTapTime]' and '$tapSchPlg[startTapTime]'")->where('date',date('Y-m-d'))->first();

		if(!empty($tapSch)){
			$tap = Taps::where('idTapSch',$tapSch->id)->where('idCardUser',$cardUser->id)->first();
			if ($tap->status == 0) {
				//jika terlambat
				if (!empty($tapSchLate)) {
					$updateLate = array('status' => 2);
					Taps::where('idTapSch',$tapSchLate->id)->where('idCardUser',$cardUser->id)->update($updateLate);
					return array('message' => 'm004: '.date('H:i:s')." (TERLAMBAT) ".$cardUser->name);
				}
				//jika berhasil
				$updateData = array('status' => 1);
				$updateStatus = Taps::where('idTapSch',$tapSch->id)->where('idCardUser',$cardUser->id)->update($updateData);
				if($updateStatus==1){
					$message ='siswa dengan nama '.$cardUser->name.' telah presensi disekolah pada '.date('l jS F Y H:i:s');
					$notif = Notif::where('id',$cardUser->idNotif)->first();
					$to = $notif->phone;
					//with trigger
					$output  = DB::connection('sms')->table('outbox')->insert( array( 'DestinationNumber' => $to, 'TextDecoded' => $message));
					if($output){
						$sms = "WORKS";
					}else{
						$sms = "FAILED";
					}
					$time_end = microtime(true);
					$exe = ($time_end-$time_start)*1000;
					return array('message' => 'm007: '.date('H:i:s')." ".$cardUser->name." sms: ".$sms." ".number_format($exe,5));
				}
			}
			if ($tap->status == 1 || $tap->status == 2 ) {
				return array('message' => 'm008: '.date('H:i:s')." ".$cardUser->name);
			}
		}else{
			return array('message' => 'm005:tapping schedule is not available for now');
		}
	
		return array('message' => 'm011:something happens');

		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
				return Redirect::to('/apisms');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
