<?php

class cardController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Cards::paginate(10);
		$this->layout->content = View::make('card.index')->with('data', $data);
	}

	public function search()
	{
		$keyword = Input::get('keyword');
		$data = Cards::where('cardNum','like', "%$keyword%")->paginate(10);

		$this->layout->content = View::make('card.index')->with('data', $data)->with('keyword', $keyword);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('card.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
						'cardnum' => 'required',
						'note'	=> 'between:1,255',
						'status' => 'required'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to('card/create')->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$card = new Cards;

			$card->cardNum = Input::get('cardnum');
			$card->status = Input::get('status');
			$card->note = Input::get('note');

			if ($card->save()) 
			{
				return Redirect::to('/card')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}

		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$card = Cards::find($id);
		$this->layout->content = View::make('card.edit')->with('card', $card);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
						'cardnum' => 'required',
						'note'	=> 'between:1,255',
						'status' => 'required'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to('card/create')->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$card = Cards::find($id);

			$card->cardNum = Input::get('cardnum');
			$card->status = Input::get('status');
			$card->note = Input::get('note');

			if ($card->save()) 
			{
				return Redirect::to('/card')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}

		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$card =  Cards::find($id);
		
		if ($card->delete()) 
		{
			return Redirect::to('card');
		}

	}


}
