<?php

class tapController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout = View::make('tap.index')->paginate(15);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$class = Group::find($id);
		$skrg = date('Y-m-d');
		 $idsch = getIdTapSch($id,$skrg,2);
		//$kueri = "select a.status,  c.name, c.idClass, c.NIS, a.updated_at FROM taps a, cardusers c WHERE c.idClass= $id AND a.idCardUser = c.id AND a.idTapSch = $idsch->id";

		$kueri = "select a.status,  c.name, c.idClass, a.idTapSch, c.NIS, a.updated_at FROM taps a, cardusers c WHERE c.idClass= $id AND a.idCardUser = c.id ";
		$cardusers = DB::select(DB::raw($kueri));
		  $schedules = TapSch::where('date', $skrg)->get();

		//$cardusers = CardUsers::where('idClass', $id) ->paginate(30);
		//$schedules = TapSch::where('idClass', $id)->paginate(30);
		
		$this->layout->content = View::make('tap.detail')->with('class', $class)->with('schedules', $schedules)->with('shows',$cardusers);
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function search($id)
	{
		$range = Input::get('rangedate');

		$idclass = 0;
		if($id == 12){
			$idclass = 1;
		}
		
		if ($range!="") 
		{
			$dates = explode(" - ", $range);
			$class = Group::find($id);
			
			//return $idsch = getIdTapSch($dates[0]);
			//$kueri = "select a.status,  c.name, c.idClass, c.NIS, a.updated_at FROM taps a, cardusers c WHERE c.idClass= $id AND a.idCardUser = c.id AND a.idTapSch = $idsch->id";
			$kueri = "select a.status,  c.name, c.idClass, a.idTapSch, c.NIS, a.updated_at, a.created_at FROM taps a, cardusers c WHERE c.idClass= $id AND a.idCardUser = c.id ";

			$cardusers = DB::select(DB::raw($kueri));
			$schedules = TapSch::whereRaw(DB::raw("date BETWEEN STR_TO_DATE('$dates[0]', '%Y-%m-%d') AND STR_TO_DATE('$dates[1]', '%Y-%m-%d')"))->where('idClass', $idclass)->orderBy('date', 'asc')->get();

			$this->layout->content = View::make('tap.detail')->with('class', $class)->with('schedules', $schedules)->with('shows',$cardusers)->with('idClass', $idclass);

			/* $idsch = getIdTapSch($id,$dates[0],$dates[1]);
			 //return count($idsch);
			$shows = array();
			foreach ($idsch as $key => $value) {
				$kueri = "select a.status,  c.name, c.idClass, c.NIS, a.updated_at FROM taps a, cardusers c WHERE c.idClass= $id AND a.idCardUser = c.id AND a.idTapSch = $value->id";
				$cardusers = DB::select(DB::raw($kueri));
				$shows[] = $cardusers;
			}
			// $shows[0];
			$schedules = TapSch::where('idClass', $id)->where('type', "datang")->whereRaw(DB::raw("date BETWEEN STR_TO_DATE('$dates[0]', '%Y-%m-%d') AND STR_TO_DATE('$dates[1]', '%Y-%m-%d')"))->get();
			$this->layout->content = View::make('tap.detail')->with('class', $class)->with('schedules', $schedules)->with('shows', $shows);*/
		}	
		else 
		{
			return Redirect::to("tap/$id");
		}

	}

	public function export($id)
	{
		$from = Input::get('from');
		$until = Input::get('until');
		

		if ($from!="") 
		{
			$idsch = getIdTapSch($id,$from,$until);
			$kueri = "select a.status,  c.name, c.idClass, a.idTapSch, c.NIS, a.updated_at FROM taps a, cardusers c WHERE c.idClass= $id AND a.idCardUser = c.id ";
			$cardusers = DB::select(DB::raw($kueri));
			$schedules = TapSch::where('idClass', $id)->where('type', "datang")->whereRaw(DB::raw("date BETWEEN STR_TO_DATE('$from', '%Y-%m-%d') AND STR_TO_DATE('$until', '%Y-%m-%d')"))->get();
		}
		else 
		{
			/*$skrg = date('Y-m-d');
			$kueri = "select a.status,  c.name, c.idClass, c.NIS, a.updated_at FROM taps a, cardusers c WHERE c.idClass= $id AND a.idCardUser = c.id AND a.idTapSch = $skrg";
			return $cardusers = DB::select(DB::raw($kueri));
			$schedules = TapSch::where('idClass', $id)->where('type', "datang")->where('date', $skrg)->get();*/
			return 'masukan tanggal!';

		}
		
		$taps = array('schedules' => $schedules,
							'carduser' => $cardusers);

		if (Input::get('type')=='pdf') 
		{
			//return $schedules;	
			
			$pdf = PDF::loadView('exporttap', $taps);
			return $pdf->download('report-taps.pdf');
		}
		elseif (Input::get('type')=='xls') 
		{
			Excel::create("exporttap", function($excel) use($taps)
			{
				$excel->sheet("report", function($sheet) use($taps)
				{
					$sheet->loadView('exporttap',$taps);
				});
			})->download('xls');
		}

	}

}
