<?php

class dashboardController extends \BsysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout = View::make('dashboard.index');
	}

	public function broadcast()
	{
		$nowTime = date('Y-m-d');
		$idjadwal = getIdTapSch($nowTime);
		$idclass = getIdClass($nowTime);
		$hadir = array();
		$tdkhadir = array();
		$kls = Group::where('name', 'LIKE', '%X %')->orderBy('name', 'desc')->get();
		
		$murid = array();
		$c = array(); 
		$nama = array(); 
//		return $kls[0]['id'];
		$a = $idjadwal[0]['id'];
		//for($i=1; $i <= count($kls); $i++){
		foreach ($kls as $key => $value) {

			//$query = "select a.status,  c.name, c.idClass, a.idTapSch, c.NIS, a.updated_at FROM taps a, cardusers c WHERE c.idClass= $i AND a.idTapSch = $a AND a.idCardUser = c.id ";
			//$semua = DB::select(DB::raw($query));
			$tdkhadir[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
							->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
							->where('taps.status', '=', 0)
							->where('taps.idTapSch', '=', $a)
							->where('cardusers.idClass', '=', $value->id)
							->get();

			$hadir[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
						->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
						->where('taps.status', '=', 1)
						->where('taps.idTapSch', '=', $a)
						->where('cardusers.idClass', '=', $value->id)
						->get();


			$telat[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
						->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
						->where('taps.status', '=', 2)
						->where('taps.idTapSch', '=', $a)
						->where('cardusers.idClass', '=', $value->id)
						->get();

			$jumhadir[$value->id] = count($hadir[$value->id]);
			$jumtdkhadir[$value->id] = count($tdkhadir[$value->id]);
			$jumtelat[$value->id] = count($telat[$value->id]);

			$sum[$value->id] = $jumhadir[$value->id]+$jumtdkhadir[$value->id]+$jumtelat[$value->id];
			$hdr = $jumhadir[$value->id]+$jumtelat[$value->id];
			if($jumhadir[$value->id]!=0){
				$persenhadir[$value->id] = $hdr/$sum[$value->id]*100;
			}else{
				$persenhadir[$value->id] = 0;
			}

			foreach ($tdkhadir[$value->id] as $keys => $values) {
				$nama[$value->id][$keys] = $values->name;
			}
			

		}
		//return $jumtdkhadir[1];
		//return $jumhadir;
		//return $persenhadir;
		//return $jumtelat;
		$jumrd = array_sum($sum);
		$semuahadir = array_sum($jumhadir)+array_sum($jumtelat);
		if($jumrd != 0){
			$persensemua = $semuahadir/$jumrd*100;
		}else{
			$persensemua = 0;
		}
	
			 
		
		/*$hadir = array(0,0,0,0,0,0,0,0,0,0,0,0,0); 
		$tdkhadir = array(0,0,0,0,0,0,0,0,0,0,0,0,0); 
		$hadir[1] = 0; 
		$tdkhadir[1] = 0; 
		$persen = array(); 
		
		
		$k = 1;
		

		for($i=1; $i <= count($kls)-11; $i++){
			for($j=0; $j <= count($murid[$i])-1; $j++){
		  
			  $taps = Taps::where('idCardUser', $murid[$i][$j]['id'] )->where('status', 1)->get();
			  $ntaps = Taps::where('idTapSch', $murid[$i][$j]['id'] )->where('status', 0)->get();

			  if(count($taps)!=0){
		  		$hadir[$i] = $hadir[$i]+1;
			  }else{
			  	$tdkhadir[$i] = $tdkhadir[$i]+1;
			  }

			  	$r[$i] = $hadir[$i]+$tdkhadir[$i];
			  	if(count($hadir[$i])>0){
			 		$persen[$i] = $hadir[$i]/$r[$i]*100;
			 	}else{
			 		$persen[$i] = 'kosong';
			 	}	
		 	}
		}
		  //return $persen;
		return $hadir;
		 $tapsall = array_sum($b);
		 $ntapsall = array_sum($a);
		 if($tapsall > 0){
			$persentaps = ($tapsall/($tapsall+$ntapsall))*100;
		}else{
			$persentaps = 0;
		}*/

		$kelasx = Group::where('name', 'LIKE', '%X %')->orderBy('name', 'desc')->get();
		// $kelasxi = Group::where('name', 'LIKE', '%XI %')->orderBy('name', 'desc')->get();
		// $kelasxii = Group::where('name', 'LIKE', '%XII %')->orderBy('name', 'desc')->get();
		 
		//$url1=$_SERVER['REQUEST_URI'];
		$url2='/presencesys/public/rekap-broadcast2';
		header("Refresh: 15; URL=$url2");

		$array_hari = array('1'=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
		$array_bulan = array('1'=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');
		
		//return $kelasx[0];
		$this->layout->content = View::make('rekap.broadcast')->with('kelasx', $kelasx)->with('jumhadir', $jumhadir)->with('jumtdkhadir', $jumtdkhadir)->with('jumsemua', $semuahadir)->with('persenhadir', $persenhadir)->with('persensemua', $persensemua)->with('jumtelat', $jumtelat)->with('array_hari', $array_hari)->with('array_bulan', $array_bulan);
	}

	public function broadcastxi()
	{
		$nowTime = date('Y-m-d');
		$idjadwal = getIdTapSch($nowTime);
		$idclass = getIdClass($nowTime);
		$hadir = array();
		$tdkhadir = array();
		$kls = Group::where('name', 'LIKE', '%XI %')->orderBy('name', 'desc')->get();
		
		$murid = array();
		$c = array(); 
		$nama = array(); 
//		return $kls[0]['id'];
		$a = $idjadwal[0]['id'];
		//for($i=1; $i <= count($kls); $i++){
		foreach ($kls as $key => $value) {
			
			//$query = "select a.status,  c.name, c.idClass, a.idTapSch, c.NIS, a.updated_at FROM taps a, cardusers c WHERE c.idClass= $i AND a.idTapSch = $a AND a.idCardUser = c.id ";
			//$semua = DB::select(DB::raw($query));
			
			
			
			$tdkhadir[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
							->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
							->where('taps.status', '=', 0)
							->where('taps.idTapSch', '=', $a)
							->where('cardusers.idClass', '=', $value->id)
							->get();

			$hadir[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
						->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
						->where('taps.status', '=', 1)
						->where('taps.idTapSch', '=', $a)
						->where('cardusers.idClass', '=', $value->id)
						->get();


			$telat[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
						->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
						->where('taps.status', '=', 2)
						->where('taps.idTapSch', '=', $a)
						->where('cardusers.idClass', '=', $value->id)
						->get();

			$jumhadir[$value->id] = count($hadir[$value->id]);
			$jumtdkhadir[$value->id] = count($tdkhadir[$value->id]);
			$jumtelat[$value->id] = count($telat[$value->id]);

			$sum[$value->id] = $jumhadir[$value->id]+$jumtdkhadir[$value->id]+$jumtelat[$value->id];
			$hdr = $jumhadir[$value->id]+$jumtelat[$value->id];
			if($jumhadir[$value->id]!=0){
				$persenhadir[$value->id] = $hdr/$sum[$value->id]*100;
			}else{
				$persenhadir[$value->id] = 0;
			}

			foreach ($tdkhadir[$value->id] as $keys => $values) {
				$nama[$value->id][$keys] = $values->name;
			}
			

		}
		//return $jumtdkhadir[1];
		//return $jumhadir;
		//return $persenhadir;
		//return $jumtelat;
		$jumrd = array_sum($sum);
		$semuahadir = array_sum($jumhadir)+array_sum($jumtelat);
		//return $jumrd;
		if($jumrd != 0){
			$persensemua = $semuahadir/$jumrd*100;
		}else{
			$persensemua = 0;
		}

		 
		
		/*$hadir = array(0,0,0,0,0,0,0,0,0,0,0,0,0); 
		$tdkhadir = array(0,0,0,0,0,0,0,0,0,0,0,0,0); 
		$hadir[1] = 0; 
		$tdkhadir[1] = 0; 
		$persen = array(); 
		
		
		$k = 1;
		

		for($i=1; $i <= count($kls)-11; $i++){
			for($j=0; $j <= count($murid[$i])-1; $j++){
		  
			  $taps = Taps::where('idCardUser', $murid[$i][$j]['id'] )->where('status', 1)->get();
			  $ntaps = Taps::where('idTapSch', $murid[$i][$j]['id'] )->where('status', 0)->get();

			  if(count($taps)!=0){
		  		$hadir[$i] = $hadir[$i]+1;
			  }else{
			  	$tdkhadir[$i] = $tdkhadir[$i]+1;
			  }

			  	$r[$i] = $hadir[$i]+$tdkhadir[$i];
			  	if(count($hadir[$i])>0){
			 		$persen[$i] = $hadir[$i]/$r[$i]*100;
			 	}else{
			 		$persen[$i] = 'kosong';
			 	}	
		 	}
		}
		  //return $persen;
		return $hadir;
		 $tapsall = array_sum($b);
		 $ntapsall = array_sum($a);
		 if($tapsall > 0){
			$persentaps = ($tapsall/($tapsall+$ntapsall))*100;
		}else{
			$persentaps = 0;
		}*/

		//$kelasx = Group::where('name', 'LIKE', '%X %')->orderBy('name', 'desc')->get();
		 $kelasxi = Group::where('name', 'LIKE', '%XI %')->orderBy('name', 'desc')->get();
		 //$kelasxii = Group::where('name', 'LIKE', '%XII %')->orderBy('name', 'desc')->get();
		 $array_hari = array('1'=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
		$array_bulan = array('1'=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');

		//$url1=$_SERVER['REQUEST_URI'];
		$url2='/presencesys/public/rekap-broadcast2xi';
		header("Refresh: 15; URL=$url2");
		
		//return $kelasxi;
		$this->layout->content = View::make('rekap.broadcastxi')->with('kelasxi', $kelasxi)->with('jumhadir', $jumhadir)->with('jumtdkhadir', $jumtdkhadir)->with('jumsemua', $semuahadir)->with('persenhadir', $persenhadir)->with('persensemua', $persensemua)->with('jumtelat', $jumtelat)->with('array_hari', $array_hari)->with('array_bulan', $array_bulan);
	}

	public function broadcastxii()
	{
		$nowTime = date('Y-m-d');
		$idjadwal = getIdTapSch($nowTime);
		$idclass = getIdClass($nowTime);
		$hadir = array();
		$tdkhadir = array();
		$kls = Group::where('name', 'LIKE', '%XII %')->orderBy('name', 'desc')->get();
		
		$murid = array();
		$c = array(); 
		$nama = array(); 
//		return $kls[0]['id'];
		$a = $idjadwal[0]['id'];
		//for($i=1; $i <= count($kls); $i++){
		foreach ($kls as $key => $value) {
			
			//$query = "select a.status,  c.name, c.idClass, a.idTapSch, c.NIS, a.updated_at FROM taps a, cardusers c WHERE c.idClass= $i AND a.idTapSch = $a AND a.idCardUser = c.id ";
			//$semua = DB::select(DB::raw($query));
			
			
			
			$tdkhadir[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
							->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
							->where('taps.status', '=', 0)
							->where('taps.idTapSch', '=', $a)
							->where('cardusers.idClass', '=', $value->id)
							->get();

			$hadir[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
						->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
						->where('taps.status', '=', 1)
						->where('taps.idTapSch', '=', $a)
						->where('cardusers.idClass', '=', $value->id)
						->get();


			$telat[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
						->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
						->where('taps.status', '=', 2)
						->where('taps.idTapSch', '=', $a)
						->where('cardusers.idClass', '=', $value->id)
						->get();

			$jumhadir[$value->id] = count($hadir[$value->id]);
			$jumtdkhadir[$value->id] = count($tdkhadir[$value->id]);
			$jumtelat[$value->id] = count($telat[$value->id]);

			$sum[$value->id] = $jumhadir[$value->id]+$jumtdkhadir[$value->id]+$jumtelat[$value->id];
			$hdr = $jumhadir[$value->id]+$jumtelat[$value->id];
			if($jumhadir[$value->id]!=0){
				$persenhadir[$value->id] = $hdr/$sum[$value->id]*100;
			}else{
				$persenhadir[$value->id] = 0;
			}

			foreach ($tdkhadir[$value->id] as $keys => $values) {
				$nama[$value->id][$keys] = $values->name;
			}
			

		}
		//return $jumtdkhadir[1];
		//return $jumhadir;
		//return $persenhadir;
		//return $jumtelat;
		$jumrd = array_sum($sum);
		$semuahadir = array_sum($jumhadir)+array_sum($jumtelat);

		$persensemua = $semuahadir/$jumrd*100;

		
		/*$hadir = array(0,0,0,0,0,0,0,0,0,0,0,0,0); 
		$tdkhadir = array(0,0,0,0,0,0,0,0,0,0,0,0,0); 
		$hadir[1] = 0; 
		$tdkhadir[1] = 0; 
		$persen = array(); 
		
		
		$k = 1;
		

		for($i=1; $i <= count($kls)-11; $i++){
			for($j=0; $j <= count($murid[$i])-1; $j++){
		  
			  $taps = Taps::where('idCardUser', $murid[$i][$j]['id'] )->where('status', 1)->get();
			  $ntaps = Taps::where('idTapSch', $murid[$i][$j]['id'] )->where('status', 0)->get();

			  if(count($taps)!=0){
		  		$hadir[$i] = $hadir[$i]+1;
			  }else{
			  	$tdkhadir[$i] = $tdkhadir[$i]+1;
			  }

			  	$r[$i] = $hadir[$i]+$tdkhadir[$i];
			  	if(count($hadir[$i])>0){
			 		$persen[$i] = $hadir[$i]/$r[$i]*100;
			 	}else{
			 		$persen[$i] = 'kosong';
			 	}	
		 	}
		}
		  //return $persen;
		return $hadir;
		 $tapsall = array_sum($b);
		 $ntapsall = array_sum($a);
		 if($tapsall > 0){
			$persentaps = ($tapsall/($tapsall+$ntapsall))*100;
		}else{
			$persentaps = 0;
		}*/

		$kelasx = Group::where('name', 'LIKE', '%X %')->orderBy('name', 'desc')->get();
		 $kelasxi = Group::where('name', 'LIKE', '%XI %')->orderBy('name', 'desc')->get();
		 $kelasxii = Group::where('name', 'LIKE', '%XII %')->orderBy('name', 'desc')->get();
		 
		 $array_hari = array('1'=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
		$array_bulan = array('1'=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');

		//$url1=$_SERVER['REQUEST_URI'];
		$url2='/presencesys/public/rekap-broadcast2xii';
		header("Refresh: 15; URL=$url2");
		
		//return $kelasx[0];
		$this->layout->content = View::make('rekap.broadcastxii')->with('kelasxii', $kelasxii)->with('jumhadir', $jumhadir)->with('jumtdkhadir', $jumtdkhadir)->with('jumsemua', $semuahadir)->with('persenhadir', $persenhadir)->with('persensemua', $persensemua)->with('jumtelat', $jumtelat)->with('array_hari', $array_hari)->with('array_bulan', $array_bulan);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function kehadiran()
	{
		$nowTime = date('Y-m-d');
		$idjadwal = getIdTapSch($nowTime);
		$idclass = getIdClass($nowTime);
		$hadir = array();
		$tdkhadir = array();
		$kls = Group::where('name', 'LIKE', '%X %')->orderBy('name', 'desc')->get();
		
		$murid = array();
		$c = array(); 
		$nama = array(); 
		$namaTelat = array(); 
		
		$a = $idjadwal[0]['id'];
		foreach ($kls as $key => $value) {
			
			$tdkhadir[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
							->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
							->where('taps.status', '=', 0)
							->where('taps.idTapSch', '=', $a)
							->where('cardusers.idClass', '=', $value->id)
							->get();

			
			$telat[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
						->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
						->where('taps.status', '=', 2)
						->where('taps.idTapSch', '=', $a)
						->where('cardusers.idClass', '=', $value->id)
						->get();

			foreach ($tdkhadir[$value->id] as $keys => $values) {
				$nama[$value->id][$keys] = $values->name;
			}
			if($telat[$value->id] == null){
				$namaTelat[$value->id][0] = 'Tidak ada yang terlambat';
			}else{
				foreach ($telat[$value->id] as $keys2 => $values2) {
					$namaTelat[$value->id][$keys2] = $values2->name;
				}
			}

		}
		
		$kelasx = Group::where('name', 'LIKE', '%X %')->orderBy('name', 'desc')->get();
		//return $namaTelat;
		//return $nama;
		$url2='/presencesys/public/rekap-broadcastxi';
		header("Refresh: 15; URL=$url2");
		
		$array_hari = array('1'=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
		$array_bulan = array('1'=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');
		
		$this->layout->content = View::make('rekap.broadcast2')->with('kelasx', $kelasx)->with('telat', $telat)->with('name', $nama)->with('nameTelat', $namaTelat)->with('array_hari', $array_hari)->with('array_bulan', $array_bulan);
	
	}


	public function kehadiranxi()
	{
		$nowTime = date('Y-m-d');
		$idjadwal = getIdTapSch($nowTime);
		$idclass = getIdClass($nowTime);
		$hadir = array();
		$tdkhadir = array();
		$kls = Group::where('name', 'LIKE', '%XI %')->orderBy('name', 'desc')->get();
		
		$murid = array();
		$c = array(); 
		$nama = array(); 
		$namaTelat = array(); 
		
		$a = $idjadwal[0]['id'];
		foreach ($kls as $key => $value) {
			
			$tdkhadir[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
							->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
							->where('taps.status', '=', 0)
							->where('taps.idTapSch', '=', $a)
							->where('cardusers.idClass', '=', $value->id)
							->get();

			
			$telat[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
						->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
						->where('taps.status', '=', 2)
						->where('taps.idTapSch', '=', $a)
						->where('cardusers.idClass', '=', $value->id)
						->get();

			foreach ($tdkhadir[$value->id] as $keys => $values) {
				$nama[$value->id][$keys] = $values->name;
			}
			if($telat[$value->id] == null){
				$namaTelat[$value->id][0] = 'Tidak ada yang terlambat';
			}else{
				foreach ($telat[$value->id] as $keys2 => $values2) {
					$namaTelat[$value->id][$keys2] = $values2->name;
				}
			}

		}
		//return $nama;
		//return $namaTelat;
		$kelasxi = Group::where('name', 'LIKE', '%XI %')->orderBy('name', 'desc')->get();
		
		$url2='/presencesys/public/rekap-broadcast';
		header("Refresh: 15; URL=$url2");

		$array_hari = array('1'=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
		$array_bulan = array('1'=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');
		
		$this->layout->content = View::make('rekap.broadcast2xi')->with('kelasxi', $kelasxi)->with('telat', $telat)->with('name', $nama)->with('array_hari', $array_hari)->with('array_bulan', $array_bulan)->with('nameTelat', $namaTelat);
	}


	public function kehadiranxii()
	{
		$nowTime = date('Y-m-d');
		$idjadwal = getIdTapSch($nowTime);
		$idclass = getIdClass($nowTime);
		$hadir = array();
		$tdkhadir = array();
		$kls = Group::where('name', 'LIKE', '%XII %')->orderBy('name', 'desc')->get();
		
		$murid = array();
		$c = array(); 
		$nama = array(); 
		
		$a = $idjadwal[0]['id'];
		foreach ($kls as $key => $value) {
			
			$tdkhadir[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
							->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
							->where('taps.status', '=', 0)
							->where('taps.idTapSch', '=', $a)
							->where('cardusers.idClass', '=', $value->id)
							->get();

			
			$telat[$value->id] = DB::table('taps')->join('cardusers', 'taps.idCardUser', '=', 'cardusers.id')
						->select('taps.status','cardusers.name','cardusers.idClass','taps.idTapSch','cardusers.NIS', 'taps.updated_at')
						->where('taps.status', '=', 2)
						->where('taps.idTapSch', '=', $a)
						->where('cardusers.idClass', '=', $value->id)
						->get();

			foreach ($tdkhadir[$value->id] as $keys => $values) {
				$nama[$value->id][$keys] = $values->name;
			}
			

		}
		$kelasxii = Group::where('name', 'LIKE', '%XII %')->orderBy('name', 'desc')->get();

	  	$url2='/presencesys/public/rekap-broadcast';
		header("Refresh: 15; URL=$url2");

		$array_hari = array('1'=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
		$array_bulan = array('1'=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');

		$this->layout->content = View::make('rekap.broadcast2xii')->with('kelasxii', $kelasxii)->with('telat', $telat)->with('name', $nama)->with('array_hari', $array_hari)->with('array_bulan', $array_bulan);
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function show($id)
	{
		//
	}

	public function maintenance()
	{
		$this->layout->content = View::make('rekap.maintenance');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
