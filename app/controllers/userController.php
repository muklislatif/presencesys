<?php

class userController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::paginate(10);
		$akun = User::find(Sentry::getUser()->id);
		$this->layout->content = View::make('user.index')->with('users', $users)->with('akun', $akun);
	}

	public function search()
	{
		$keyword = Input::get('keyword');
		$users = User::where('name', 'like', "%$keyword%")->paginate(10);
		$akun = User::find(Sentry::getUser()->id);
		$this->layout->content = View::make('user.index')->with('users', $users)->with('keyword', $keyword)->with('akun', $akun);	
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$akun = User::find(Sentry::getUser()->id);

		$this->layout->content = View::make('user.create')->with('akun', $akun);
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
						'username'	=> 'required|unique:users',
						'email' => 'required|email',
						'password'	=> 'required',
						'cpassword'	=> 'required|same:password',
						'name'		=> 'required'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
		    return $messages = $validation->messages();
		}
		else
		{
			try
			{
			    // Create the user
			    $user = Sentry::register(array(
			        'username'     => Input::get('username'),
			        'email'	=> Input::get('email'),
			        'password'	=> Input::get('password'),
			        'name'	=> Input::get('name')
			        ), true);

			    // Find the group using the group id
			    $adminGroup = Sentry::findGroupByName('admin');

			    // Assign the group to the user
			    $user->addGroup($adminGroup);

			    return Redirect::to('/admin');
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
			    echo 'Login field is required.';
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
			    echo 'Password field is required.';
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    echo 'User with this login already exists.';
			}
			catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
			{
			    echo 'Group was not found.';
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		$akun = User::find(Sentry::getUser()->id);
		$this->layout->content = View::make('user.edit')->with('user', $user)->with('akun', $akun);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$akun = User::find(Sentry::getUser()->id);
		$user = User::find($id);
		
	    	
	    	$rules = array(
							'name'		=> 'required',
							'username'	=> 'required',
							'email' => 'required|email',
							'password'	=> 'required',
							'cpassword'	=> 'required|same:password'
					      );
	    


	    $validation = Validator::make(Input::all(),$rules);
        if ($validation->fails())
		{
		    return $messages = $validation->messages();
		}
		else
		{
			try
			{
				
				$user = Sentry::getUserProvider()->findById($id);
				
				$user->name = Input::get('name');
				$user->username = Input::get('username');
				$user->email = Input::get('email');
				$user->password = Input::get('password');

				$user->save();
			    return Redirect::to('admin');
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
			    echo 'Login field is required.';
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
			    echo 'Password field is required.';
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    echo 'User with this login already exists.';
			}
			catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
			{
			    echo 'Group was not found.';
			}
		}
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);

		if ($user->delete()) 
		{
			return Redirect::to('admin');
		}
	}


}
