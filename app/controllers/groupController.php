<?php

class groupController extends \sysBaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// return $this->akun;
		$class = Group::paginate(10);
		$this->layout->content = View::make('group.index')->with('class', $class);
	}

	public function search()
	{
		$keyword = Input::get('keyword');
		$class = Group::where('name', 'like', "%$keyword%")->paginate(10);
		$this->layout->content = View::make('group.index')->with('class', $class)->with('keyword', $keyword);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('group.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
						'name' => 'required',
						'note'	=> 'between:1,255'
				      );

        $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
			return Redirect::to('class/create')->with('message', 'data belum terisi dengan benar')->with('type', 2)->withInput();
		    //return $messages = $validation->messages();
		}
		else
		{
			$group = new Group;

			$group->name = Input::get('name');
			$group->note = Input::get('note');

			if ($group->save()) 
			{
				return Redirect::to('/class')->with('message', 'Kategori telah ditambahkan')->with('type', 1);
			}
			else 
			{
				return 'feyil';
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$class = Group::find($id);
		$students = CardUsers::where('idClass', $id)->get();
		$addstudents = CardUsers::where('idClass', 0)->get();
		$this->layout->content = View::make('group.edit')->with('class', $class)->with('students', $students)->with('addstudents', $addstudents);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$addstudent = Input::get('studentadd');
		$name = Input::get('name');
		$note = Input::get('note');
		$alamat = '/class/'.$id.'/edit';

		if( $addstudent == ''){

			$class = Group::find($id);
			$class->name = Input::get('name');
			$class->note = Input::get('note');
			if ($class->save())
				{
					if ($addstudent !== '') {
						DB::table('cardUsers')
				            ->whereIn('id', $addstudent)
				            ->update(array('idClass' => $id));
						return Redirect::to($alamat)->with('message', 'Kategori telah ditambahkan')->with('type', 1);
					}else{
						return Redirect::to($alamat)->with('message', 'Kategori telah ditambahkan')->with('type', 1);
					}

				}

		}else {
			DB::table('cardUsers')
	            ->whereIn('id', $addstudent)
	            ->update(array('idClass' => $id));
			return Redirect::to($alamat)->with('message', 'Kategori telah ditambahkan')->with('type', 1);
		}
			
		

	//	return Input::get('studentadd');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function remove()
	{
	
		//$class = Group::find($id);
		/*$students = CardUsers::find($id);
		$students->idClass = 0;
		if ($students->save()) 
		{
			return Redirect::to('class');
		}*/
		$id = Input::get('id');
		 $kelas = Input::get('idclass');
		$alamat = 'class/'.$kelas.'/edit';
		//return $id[0];
		if($id[0] === 'all'){
			$q = "update cardusers set idClass=19 where idClass = $kelas";
			$run = DB::select(DB::raw($q));
			return Redirect::to($alamat);
			
		}else{
			foreach ($id as $key => $value) {
				$delsch = "update cardusers set idClass=0 where id = $value";
				$tapsch = DB::select(DB::raw($delsch));
			}
			return Redirect::to($alamat);
		}
			
				
		


	}

	public function hapus($id)
	{
	
		//$class = Group::find($id);
		/*$students = CardUsers::find($id);
		$students->idClass = 0;
		if ($students->save()) 
		{
			return Redirect::to('class');
		}*/
		$delsch = "delete from class where id = $id";
		$a = "update cardusers set idClass=0 where idClass = $id";
		$b = DB::select(DB::raw($a));
		$tapsch = DB::select(DB::raw($delsch));

		return Redirect::to('class');


	}


}
