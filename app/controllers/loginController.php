<?php

class loginController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('login');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
						'username'	=> 'required',
						'password'	=> 'required'
				      );

       /* $validation = Validator::make(Input::all(),$rules);

        if ($validation->fails())
		{
		    return $messages = $validation->messages();
		}
		else
		{*/
			try
            {
                $user = Sentry::authenticate(array(
						    'username'    => Input::get('username'),
						    'password' => Input::get('password'),
						));

				return Redirect::to('/tapsch');
            }
            catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
			    return Redirect::to('/login')->withErrors(array('login' => "Login field is required."));
			    //echo 'Login field is required.';
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
				return Redirect::to('/login')->withErrors(array('login' => "Password field is required."));
			    //echo 'Password field is required.';
			}
			catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
			{

			    //return Redirect::to('/login')->with('message', 'Wrong password, try again.')->with('type', 1)->with('validation',false);	
			    return Redirect::to('/login')->withErrors(array('login' => "Wrong password, try again."));
			    //echo 'Wrong password, try again.';

			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    return Redirect::to('/login')->withErrors(array('login' => "User was not found."));
			    //echo 'User was not found.';
			}
			catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
			{
			    return Redirect::to('/login')->withErrors(array('login' => "User is not activated."));
			    //echo 'User is not activated.';
			}
		//}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		
			Sentry::logout();
			Auth::logout();
			return Redirect::to('/login');
		
	}


}
