<?php

class Group extends Eloquent
{	
	protected $table = 'class';

	use SoftDeletingTrait;

	public function students()
	{
		return $this->hasMany('CardUsers', 'idGroup');
	}

	public function schedule()
	{
		return $this->hasMany('TapSch', 'idClass');
	}

}

?>