<?php

class CardUsers extends Eloquent
{	
	protected $table = 'cardUsers';
	use SoftDeletingTrait;

	public function group()
	{
		return $this->belongsTo('Group', 'idClass');
	}

	public function card()
	{
		return $this->belongsTo('Cards', 'idCard');
	}

	public function notif()
	{
		return $this->belongsTo('Notif', 'idNotif');
	}
}

?>