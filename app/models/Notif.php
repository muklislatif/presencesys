<?php

class Notif extends Eloquent
{	
	protected $table = 'notif';

	use SoftDeletingTrait;

	public function carduser()
	{
		return $this->hasOne('CardUsers', 'idNotif');
	}
}

?>