<?php

class TapSch extends Eloquent
{	
	protected $table = 'tapSch';
	
	use SoftDeletingTrait;

	public function group()
	{
		return $this->belongsTo('Group', 'idClass');
	}

	public function taps()
	{
		return $this->hasMany('Taps', 'idTapSch');
	}
}

?>