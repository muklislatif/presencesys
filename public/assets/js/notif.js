$(document).ready(function()
{
	if($("#notification").length>0)
	{
		$("#notification").fadeIn();
		
		setTimeout(function() {
			hideNotif();
		}, 5000);
	}
})

function hideNotif()
{
	$("#notification").fadeOut(500);
}